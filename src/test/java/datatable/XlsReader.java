/*  

    XlsReader is a java class which is responsible for all the communication with the .xls file. 
    It provides methods for reading data from the spreadsheet.  

 */

package datatable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class XlsReader 
{
	public  String path;
	public String path2 = "C:\\Users\\Lenovo\\Desktop\\TheatroWebportalAutomation\\src\\test\\java\\config\\testDatacopy.xls";
	public  FileInputStream fis = null;

	/*   
	 public Xls_Reader(String path) method specification :-

	 1)  Specify the path of the xls file and set a file input stream with the file.
	 2)  path -> Path of the xls file 
	 3)  FileInputStream -> To take input from specified xls file.

	 */

	public XlsReader(String path) 
	{
		this.path=path;
		try 
		{
			fis = new FileInputStream(path);
			fis.close();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 	
	}
	
	/*   
	 public String getFirstSheetname() method specification :-

	 1) Returns first sheet name  from specified excel sheet
	 2) Workbook -> Refer to the whole xls workbook file
	 3) Workbook.getWorkbook(<xls workbook file>) -> Load the xls workbook flie
	 4) w.getSheet(0).getName() -> Give the first sheet name

	 */
	
	public String getFirstSheetname() throws IOException, BiffException
	{

		File inputWorkbook = new File(path);
		Workbook w;
		w = Workbook.getWorkbook(inputWorkbook);
		String  sheet = w.getSheet(0).getName();
		
		return sheet;
		
	}
	
	/*   
	 public int getRowCount(String sheetName) method specification :-

	 1)  Return total number of rows in specified excel sheet.
	 2)  sheetName -> name of the excel sheet. 
	 3)  Workbook -> Refer to the whole xls file.
	 4)  Sheet -> Refer to an individual sheet within that workbook.
	 5)  sheet.getRows() -> returns total number of rows present within a particular sheet.

	 */

	public int getRowCount(String sheetName) throws BiffException, IOException // it will count the number of rows in the controller sheet
	{
		File inputWorkbook = new File(path);
		Workbook w;
		w = Workbook.getWorkbook(inputWorkbook);
		Sheet sheet = w.getSheet(sheetName); 
		int row_count=0;
		for (int i = 0; i < sheet.getRows(); i++) 
		{
			row_count++;
		} 
		return row_count;
	}
	
	
	/*   
	 public String getCellData(String sheetName, String colName,int rowNum) method specification :-

	 1)  Return cell data (string format) from specified excel sheet.
	 2)  sheetName -> name of the excel sheet. 
	 3)  colName, rowNum -> column name and row no. respectively within the sheet. 
	 4)  Workbook -> Refer to the whole xls file.
	 5)  Sheet -> Refer to an individual sheet within that workbook.
	 6)  sheet.getCell() -> Used to hold the control of a particular cell specified by colNum and rowNum.
	 7)  cell.getContents() -> return content of a cell.

	 */
	
	public String getCellData(String sheetName, String colName,int rowNum) throws BiffException, IOException // it will count the number of rows in the controller sheet
	{
		File inputWorkbook = new File(path);
		Workbook w;
		w = Workbook.getWorkbook(inputWorkbook);
		Sheet sheet = w.getSheet(sheetName);
		int i = 0;
		int colNum = 0;
		
		for(i=0; i<sheet.getColumns(); i++){
			if(sheet.getCell(i, 0).getContents().equals(colName)){
				break;
			}
		}
			
		colNum = i;
		String cellData = sheet.getCell(colNum,rowNum).getContents();
		return cellData;
	}
	
	public void setCellData(String sheetName, String colName,int rowNum,String inputText) throws BiffException, IOException, RowsExceededException, WriteException // it will count the number of rows in the controller sheet
	{
		File inputWorkbook = new File(path);
		//System.out.println(path);
		WritableWorkbook w;
		Workbook wb = Workbook.getWorkbook(inputWorkbook);
		//System.out.println("inputWorkbook");
		w = wb.createWorkbook(inputWorkbook,wb);
		System.out.println(sheetName);
		WritableSheet sheet = w.getSheet(sheetName);
		System.out.println(sheetName+inputText);
		int i = 0;
		int colNum = 0;
		
		for(i=0; i<sheet.getColumns(); i++){
			if(sheet.getCell(i, 0).getContents().equals(colName)){
				break;
			}
		}
			
		colNum = i;
		Label text = new Label(colNum,rowNum,inputText);
		System.out.println(text);
		sheet.addCell(text);
		System.out.println(sheetName);
		
		w.write();
		w.close();
		
		
		
		
	}
	
}