package reports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.lang3.time.StopWatch;

import testscripts.DriverScript;

public class ReportUtil extends DriverScript{
    public static String currentDir;
    public static String indexResultFileName;
    public static String currentSuiteName;
    public static int tsid;
    public static int scriptNumber = 1;
    
    public static ArrayList < String > description = new ArrayList < String > ();;
    public static ArrayList < String > keyword = new ArrayList < String > ();;
    public static ArrayList < String > teststatus = new ArrayList < String > ();;
    public static ArrayList < String > screenShotPath = new ArrayList < String > ();;
    public static ArrayList < StopWatch > TotalTimeTaken = new ArrayList < StopWatch > ();;
    
	public static void startTesting(String fileName, String env, String rel, String browser, String testSiteUrl) {
       indexResultFileName = fileName;
		currentDir = indexResultFileName.substring(0, fileName.lastIndexOf("/"));
       File report = new File(currentDir);
       // Create a Report Folder if not already present
       if(!report.exists()){
    	   report.mkdir();
       }
       
       else{
    	   // Clear Test Report folder
           File[] allFiles = report.listFiles();
           for (int i = 0; i < allFiles.length; i++) {
               allFiles[i].delete();
           }
       }
           // Start generating Test report
           FileWriter fStream = null;
           BufferedWriter out = null;
           try {
               // Create file 
               fStream = new FileWriter(indexResultFileName);
               out = new BufferedWriter(fStream);
               out.newLine();
               
               out.write("<html>\n");
               out.write("<HEAD>\n");
               out.write(" <TITLE>Theatro Web portal test report</TITLE>\n");
               out.write("</HEAD>\n");
               out.write("</html>");

               out.write("<body>\n");
               out.write("<h4 align=center><FONT SIZE=6><b><u> Theatro Web portal automation test report </b></u></h4>\n");
               out.write("<table border=1 cellspacing=1 cellpadding=1>\n");
               out.write("<tr>\n");

               out.write("<h4> <FONT SIZE=4> <u>Test Details :</u></h4>\n");
               out.write("</tr>\n");
               
               out.write("<td width=150 align= left  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2.75><b>Environment</b></td>\n");
               out.write("<td width=150 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75>" + env + "</b></td>\n");
               out.write("</tr>\n");
               out.write("<tr>\n");

               out.write("<td width=150 align= left  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2.75><b>Release</b></td>\n");
               out.write("<td width=150 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>" + rel + "</b></td>\n");
               out.write("</tr>\n");
               
               out.write("<td width=150 align= left  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2.75><b>Browser</b></td>\n");
               out.write("<td width=150 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75>" + browser + "</b></td>\n");
               out.write("</tr>\n");

               out.write("<td width=150 align= left  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2.75><b>Application URL</b></td>\n");
               out.write("<td width=150 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>" + testSiteUrl + "</b></td>\n");
               out.write("</tr>\n");
               out.write("</table>\n");
               
               
             //Close the output stream
               out.close();
           }
           catch (Exception e) { //Catch exception if any
               System.err.println("Error: " + e.getMessage());
       }
           finally {

               fStream = null;
               out = null;
           }
    }

	public static void startSuite(String suiteName) {

		currentSuiteName = suiteName;
        FileWriter fStream = null;
        BufferedWriter out = null;
        tsid = 1;
        
        try {
			fStream = new FileWriter(indexResultFileName, true);
			out = new BufferedWriter(fStream);
			out.write("<h4> <FONT SIZE=4.5> <u>" + suiteName + " Report :</u></h4>\n");
            out.write("<table  border=1 cellspacing=1 cellpadding=1 width=100%>\n");
            out.write("<tr>\n");
            out.write("<td width=10%  align= center  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>Test Script</b></td>\n");
            out.write("<td width=40% align= center  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>Feature</b></td>\n");
            out.write("<td width=10% align= center  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>Status</b></td>\n");

            out.write("</tr>\n");
            out.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        } finally {

            fStream = null;
            out = null;
        }
			
	}

		public static void addKeyword(String desc, String key, String stat, String path, StopWatch stopwatch) {

	        description.add(desc);
	        keyword.add(key);
	        teststatus.add(stat);
	        screenShotPath.add(path);
	        TotalTimeTaken.add(stopwatch);
	        

	    }
		
		public static void addTestCase(String testSuiteName, String status) {
			 FileWriter fStream = null;
		        BufferedWriter out = null;
			try {
	            // build the keywords page
	            if (status.equalsIgnoreCase("Pass") || status.equalsIgnoreCase("Fail")) {

	                File f = new File(currentDir + "//" + currentSuiteName + "_TS" + tsid + "_" + testSuiteName + ".html");
	                f.createNewFile();
	                fStream = new FileWriter(currentDir + "//" + currentSuiteName + "_TS" + tsid + "_" + testSuiteName + ".html");
	                out = new BufferedWriter(fStream);
	                out.write("<html>");
	                out.write("<head>");
	                out.write("<title>");
	                out.write(testSuiteName + " Detailed Reports");
	                out.write("</title>");
	                out.write("</head>");
	                out.write("<body>");


	                out.write("<h4> <FONT SIZE=4.5> Detailed Report :</h4>");
	                out.write("<table  border=1 cellspacing=1    cellpadding=1 width=100%>");
	                out.write("<tr> ");
	                out.write("<td align=center width=10%  align=center bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE=Arial SIZE=2><b>Step</b></td>");
	                out.write("<td align=center width=50% align=center bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE=Arial SIZE=2><b>Description</b></td>");
	                out.write("<td align=center width=10% align=center bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE=Arial SIZE=2><b>Keyword</b></td>");
	                out.write("<td align=center width=15% align=center bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE=Arial SIZE=2><b>Result</b></td>");
	                out.write("<td align=center width=15% align=center bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE=Arial SIZE=2><b>Screen Shot</b></td>");
	                out.write("<td align=center width=15% align=center bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE=Arial SIZE=2><b>Execution Time</b></td>");
	                out.write("</tr>");
	                if (description != null) {
	                    for (int i = 0; i < description.size(); i++) {
	                        out.write("<tr> ");

	                        out.write("<td align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=1><b>TS" + (i + 1) + "</b></td>");
	                        out.write("<td align=center width=50%><FONT COLOR=#153E7E FACE=Arial SIZE=1><b>" + description.get(i) + "</b></td>");
	                        out.write("<td align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=1><b>" + keyword.get(i) + "</b></td>");
	                        if (teststatus.get(i).startsWith("Pass")) out.write("<td width=20% align= center  bgcolor=#BCE954><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" + teststatus.get(i) + "</b></td>\n");
	                        else if (teststatus.get(i).startsWith("Fail")) out.write("<td width=20% align= center  bgcolor=Red><FONT COLOR=#153E7E FACE= Arial  SIZE=2><b>" + teststatus.get(i) + "</b></td>\n");
	                        if (screenShotPath.get(i) != null) out.write("<td align=center width=20%><FONT COLOR=#153E7E FACE=Arial SIZE=1><b><a href=" + screenShotPath.get(i) + " target=_blank>Screen Shot</a></b></td>");
	                        else out.write("<td align=center width=20%><FONT COLOR=#153E7E FACE=Arial SIZE=1></td>");
	                        out.write("<td align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=1><b>" + TotalTimeTaken.get(i) + "</b></td>");
	                        out.write("</tr>");
	                    }
	                }

	            

	            out.close();


            }

            fStream = new FileWriter(indexResultFileName, true);
            out = new BufferedWriter(fStream);

            out.write("<tr>\n");
            out.write("<td width=10% align= center ><FONT COLOR=#153E7E FACE= Arial  SIZE=2><b>" + scriptNumber + "</b></td>\n");
       
            if (status.equalsIgnoreCase("Skipped") || status.equalsIgnoreCase("Skip")) out.write("<td width=40% align= center ><FONT COLOR=#153E7E FACE= Arial  SIZE=2><b>" + testSuiteName + "</b></td>\n");
            else out.write("<td width=40% align= center ><FONT COLOR=#153E7E FACE= Arial  SIZE=2><b><a href="+currentSuiteName + "_TS" + tsid + "_" + testSuiteName + ".html>"+testSuiteName+"</a></b></td>\n");

            if (status.startsWith("Pass")) out.write("<td width=10% align= center  bgcolor=#BCE954><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" + status + "</b></td>\n");
            else if (status.startsWith("Fail")) out.write("<td width=10% align= center  bgcolor=Red><FONT COLOR=#153E7E FACE= Arial  SIZE=2><b>" + status + "</b></td>\n");
            else if (status.equalsIgnoreCase("Skipped") || status.equalsIgnoreCase("Skip")) out.write("<td width=10% align= center  bgcolor=yellow><FONT COLOR=153E7E FACE=Arial SIZE=2><b>" + status + "</b></td>\n");

            out.write("</tr>\n");
            scriptNumber++;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        description = new ArrayList < String > ();
        keyword = new ArrayList < String > ();
        teststatus = new ArrayList < String > ();
        screenShotPath = new ArrayList < String > ();
        TotalTimeTaken = new ArrayList<StopWatch> ();
    }
		
}