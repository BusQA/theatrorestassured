package util;

import java.io.File;
import java.util.ArrayList;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import testscripts.DriverScript;

public class SendMail extends DriverScript {

	public static void sendEmail() {
		final String password = "guruGreat9008";

		String status = null;
		String[] to = {"basavaraj@theatrolabs.com"};
		String[] cc = {"basu.mugali@gmail.com"};
		final String from = "basavaraj@theatrolabs.com";
		String host = "smtp.gmail.com";// or IP address

		APPLICATION_LOGS.debug("Sending reports to the team");
		
		// Get the session object
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", host);
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.user", from);
		properties.put("mail.smtp.password", password);

		Authenticator auth = new Authenticator() {
			// override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, password);
			}
		};
		
		// Create a session
		Session session = Session.getInstance(properties, auth);

		// compose the message
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			
			//Add To List
			for (int i = 0; i < to.length; i++) {

				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));

			}
			
			// Add cc List
			for (int i = 0; i < cc.length; i++) {

				message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));

			}
			
			// Append the status of automation run 
			if(testStatus.contains("Fail"))
				status = "Fail";
			else
				status = "Pass";
			
			// Set the subject of the mail
			message.setSubject( status + " : Automation Results for Theatro Web portal");

			// Set the body part of the mail
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(
					"Hi All,\n\n Please find the reports attached for Production Test.\n\n Regards, \nGurupreeth");

			// Zip reports generated during automation run
			TestUtil.zip(System.getProperty("user.dir") + "/Report/");

			// Attach zipped Report with the mail
			MimeBodyPart messageBodyPart1 = new MimeBodyPart();

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			
			File folder = new File(System.getProperty("user.dir") + "/Report");
			
			String list[] = folder.list();

		    for (int i=0, n=list.length; i<n; i++) {
		        System.out.println("Adding: " + list[i]);
		        messageBodyPart1 = new MimeBodyPart();
		        DataSource source = new FileDataSource(folder+"/"+list[i]);
		        System.out.println(folder+"/"+list[i]);
		        messageBodyPart1.setDataHandler(new DataHandler(source));
		        messageBodyPart1.setFileName(list[i]);
		        multipart.addBodyPart(messageBodyPart1);
		      }
		    

			message.setContent(multipart);

			// Send message
			Transport.send(message);
			APPLICATION_LOGS.debug("mail sent successfully....");

		} catch (MessagingException mex) {
			mex.printStackTrace();
		}

	}
}