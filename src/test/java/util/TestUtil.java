package util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;

import testscripts.DriverScript;

import org.openqa.selenium.TakesScreenshot;



public class TestUtil extends DriverScript{

    // Capture screenshot and store
    public static void takeScreenShot(String filePath) 
    {
    	
    	File scrFile = null;
    	
        try 
        {
        	
        		scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        		
        	// Store screenshot to the path provided
        	FileUtils.copyFile(scrFile, new File(filePath));

        }
        
        catch (Throwable screenCaptureException) 
        {
        	
            // Log error
            APPLICATION_LOGS.debug("Error while taking screenshot : " +screenCaptureException.getMessage());
            System.err.println("Error while taking screenshot : " +screenCaptureException.getMessage());
            
        }

    }
    
 // make zip of reports
    public static void zip(String filepath) {
        try {
            File inFolder = new File(filepath);
            File outFolder = new File("Reports.zip");
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outFolder)));
            BufferedInputStream in = null;
            byte[] data = new byte[1000];
            String files[] = inFolder.list();
            for (int i = 0; i < files.length; i++) { in = new BufferedInputStream(new FileInputStream(inFolder.getPath() + "/" + files[i]));
                out.putNextEntry(new ZipEntry(files[i]));
                int count;
                while ((count = in .read(data, 0, 1000)) != -1) {
                    out.write(data, 0, count);
                }
                out.closeEntry();
            }
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    // get local time in string format
    public static int  getlocalhour(){

    	int hour = 0 ;
    	    	
    	// get current hour 
    	LocalDateTime currenthr = LocalDateTime.now(); 

    	hour = LocalDateTime.now().getHour();

    	System.out.println("hour: " + hour);


    	return hour;
    }
    
    
 // get current minute
    
    // get local time in string format
    public static int  getlocalmin(){

    	int min = 0 ;
    	    	
    	// get current hour 
    	LocalDateTime currenthr = LocalDateTime.now(); 

    	min = LocalDateTime.now().getMinute();

    	System.out.println("min: " + min);
    	 	   	
    	
    	return min;
    }
    
    
 // get current second
    
    // get local time in string format
    public static int  getlocalsec(){

    	int sec = 0 ;
    	    	
    	// get current hour 
    	LocalDateTime currenthr = LocalDateTime.now(); 

    	sec = LocalDateTime.now().getSecond();

    	System.out.println("sec:" + sec);
    	 	   	
    	
    	return sec;
    }
	

}
