package util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.SubjectTerm;

import testscripts.DriverScript;

public class CheckMailInbox extends DriverScript {

	public static String checkMailInbox() throws Exception {
		String line;
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore("imaps");
		if (!store.isConnected()) {
			store.connect("imap.gmail.com", 993, "theatroauto@gmail.com", "*Theatro123");
		}
		Folder folder = store.getFolder("INBOX");
		folder.open(Folder.READ_WRITE);

		APPLICATION_LOGS.debug("Total Message:" + folder.getMessageCount());
		APPLICATION_LOGS.debug("Unread Message:" + folder.getUnreadMessageCount());

		Message[] messages = null;
		boolean isMailFound = false;
		Message mailFromTheatro = null;

		// Search for mail from God
		for (int i = 0; i < 5; i++) {
			messages = folder.search(new SubjectTerm("Welcome Aboard"), folder.getMessages());
			// Wait for 10 seconds
			if (messages.length == 0) {
				Thread.sleep(10000);
			}
		}

		// Search for unread mail from Theatro
		// This is to avoid using the mail for which
		// Registration is already done
		for (Message mail : messages) {
			if (!mail.isSet(Flags.Flag.SEEN)) {
				mailFromTheatro = mail;
				APPLICATION_LOGS.debug("Message Count is: " + mailFromTheatro.getMessageNumber());
				isMailFound = true;
			}
		}

		// Test fails if no unread mail was found from Theatro
		if (!isMailFound) {
			return "Fail : Inbox doesnot contain mail from webportal after clicking on forgot password";

		} else {
			BufferedReader reader = new BufferedReader(new InputStreamReader(mailFromTheatro.getInputStream()));
			line = reader.readLine();
		}
		return line;
	}
}
