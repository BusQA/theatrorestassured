package testcases;

import static org.hamcrest.Matchers.equalTo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Properties;
import java.util.Random;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import jxl.read.biff.BiffException;

import static io.restassured.RestAssured.given;

import testscripts.DB_Handle;
import testscripts.DriverScript;


public class StoreCreate extends DriverScript {
	
	static String userName;
	static String password;
	
	public static String createStoreAPI() throws Exception {
	
		APPLICATION_LOGS.debug("Executing test case : Store Creation POST Request API");
		//DB_Handle.getStoreIDBeforeCallingAPI();
		
		userName = testData.getCellData("StoreCreate", "AuthUserName", 1);
		password = testData.getCellData("StoreCreate", "AuthPassword", 1);
		
		int start = Integer.parseInt(testData.getCellData("StoreCreate", "Start", 1));
		int end = Integer.parseInt(testData.getCellData("StoreCreate", "End", 1));
		
		for(int i=start;i<=end;i++) {
			
			System.out.println(userName);
			System.out.println(password);
			
			String url = testData.getCellData("StoreCreate", "PostAPIUrl", 1);
			String data = testData.getCellData("StoreCreate", "POSTRequestJson", 1);
			int leftLimit = 97; // letter 'a'
		    int rightLimit = 122; // letter 'z'
		    int targetStringLength = 4;
		    Random random = new Random();
		    StringBuilder buffer = new StringBuilder(targetStringLength);
		    for (int j = 0; j < targetStringLength; j++) {
		        int randomLimitedInt = leftLimit + (int) 
		          (random.nextFloat() * (rightLimit - leftLimit + 1));
		        buffer.append((char) randomLimitedInt);
		    }
		    String generatedString = buffer.toString();

		    
			data=data.replace("RANDOM", generatedString);
			System.out.println(data);
			
			RestAssured.baseURI = CONFIG.getProperty("theatroCentralURL");
			System.out.println(RestAssured.baseURI);
		
			given().auth().preemptive().basic(userName,password).contentType("application/json").
	               	body(data).
	               	when().
	               	post(url).then().log().ifError().assertThat().spec(checkStatusCodeAndContentType);
			
		}
		//DB_Handle.getStoreIDAfterCallingAPI();
		return "Pass :Store Created using store create API";
}

}
