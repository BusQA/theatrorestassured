package testcases;

import static org.hamcrest.Matchers.equalTo;

import java.util.Properties;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

import testscripts.DriverScript;

public class ManagerAppLoginAPI extends DriverScript{
	
	@Test
	public static String maLogin() throws Exception {
		
		APPLICATION_LOGS.debug("Executing test case : MA Login API");
		
		//Properties prop=new Properties();
		
		String url = testData.getCellData("ManagerAppAPIs", "PostAPIUrlForAndroid", 1);
		
		RestAssured.baseURI = CONFIG.getProperty("theatroCentralURL");
		
		String data = testData.getCellData("ManagerAppAPIs", "POSTRequestJsonForAndroid", 1);
		
		String userName = testData.getCellData("ManagerAppAPIs", "AuthUserName", 1);
		
		String password = testData.getCellData("ManagerAppAPIs", "AuthPassword", 1);
		
		System.out.println(data);
		
		given().contentType("application/json").
		       body(data).
		       when().
		       post(url).then().log().ifError().assertThat().statusCode(200).and().header("Server", equalTo("nginx")).and()
		       .contentType("application/json");
		/*String resstring = res.asString();
		System.out.println(resstring);

		JsonPath js=new JsonPath(resstring);
		System.out.println(js.get("twResponseHeader.twsuccessMessage"));
		
		 
		given().contentType("application/json").
		body(data).
		when().
		post("/restapi/v3/app/user/login").then().log().ifError().and().header("server", equalTo("nginx"));
		*/
		
		return "Pass : MA Login API Working fine";
		
	}

}
