package testscripts;

/*import static testscripts.DriverScript.server;*/

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import jxl.read.biff.BiffException;
import util.TestUtil;

public class theatroLibrary extends DriverScript {

	// Expected page titles
	public static String expectedTitle = null;

	/* ................. Locators for the test ............... */
	public static By locatorLoginPageUserIdInput = By.id("twLoginId");
	public static By locatorLoginPagePasswordInput = By.id("twPassword");
	public static By locatorLoginPageSignInBtn = By.id("loginbtn");
	public static By locatorSideBar = By.id("sidebar");
	public static By locatorEmployeeHeader = By.cssSelector("span.portalIcon");
	public static By locatorCompanyDropdown = By.id("merchant");
	public static By locatorStoreDropdown = By.id("storeSelect");
	public static By locatorUserNameInput = By.id("userNameSearch");
	public static By locatorAddNewEmployeeLink = By.id("newEmployee");
	public static By locatorForgotPassword = By.id("forgotPwd");
	public static By locatorConfigManagementMenu = By
			.xpath(".//*[@id='nav-accordion']//span[contains(text(),'Config Management')]");
	public static By locatorFaultManagementMenu = By
			.xpath(".//*[@id='nav-accordion']/li/a/span[contains(text(),'Fault Management')]");
	public static By locatorProfileOption = By
			.xpath(".//*[@id='nav-accordion']/li/ul/li/a[contains(text(),'Profiles')]");
	public static By locatorProfileHeader = By
			.xpath(".//*[@id='main-content']/section/header[contains(text(), 'Profiles')]");
	public static By locatorAdminButtonInProfile = By.id("user-dropdown");
	public static By locatorCompanyDropDn = By.id("merchant");
	public static By locatorStoreDropDn = By.id("storeSelect");
	public static By locatorProfileDropDn = By.id("profileList");
	public static By locatorAddNewProfile = By.id("addNewProfile");
	public static By locatorNameButtonInProfile = By
			.xpath(".//*[@id='groupForm']/div/div[1]/label[contains(text(), 'Name')]");
	public static By locatorNameProfile = By.id("profileName");
	public static By locatorCommands = By.xpath(".//*[@id='groupForm']/div/div[2]/label[contains(text(), 'Commands')]");
	public static By locatorCommandList = By.id("commandList");
	public static By locatorProfileUser = By.xpath(".//*[@id='groupEmployees']//a[text()='Profile Users']");
	public static By locatorProfileUserList = By
			.xpath(".//*[@id='grpUsers']/div[contains(@class,'grp-user-Selected')]");
	public static By locatorAddProfileUser = By.id("addNewUser");
	public static By locatorDeleteProfileUser = By.id("deleteUser");
	public static By locatorSaveProfile = By.id("saveButton");
	public static By locatorOnboardingMenuOption = By.linkText("Onboarding");
	public static By locatorCompanyOptionUnderOnboardingMenu = By.linkText("Company");
	public static By locatorCompanyHeaderPanel = By.cssSelector("header.panel-heading");
	public static By locatorCompanyDropdownInCompanyPage = By.id("companies1");
	public static By locatorCompanyDetailsHeader = By
			.xpath(".//*[@id='companySelect']/div/header[contains(text(),'Company Details')]");
	public static By locatorNameField = By.id("name");
	public static By locatorNickNameField = By.cssSelector("input[name=\"nickName\"]");
	public static By locatorAddressLine1Field = By.id("streetAddress1");
	public static By locatorAddressLine2Field = By.id("streetAddress2");
	public static By locatorCityField = By.id("city");
	public static By locatorZipCodeField = By.id("zipCode");
	public static By locatorCountryDropdown = By.id("country");
	public static By locatorPrimaryContactName = By.id("primaryName");
	public static By locatorPrimaryContactMailID = By.id("primayEmailId");
	public static By locatorPrimaryContactNumber = By.id("primaryPhoneNumber");
	public static By locatorTechnicalContactName = By.id("technicalName");
	public static By locatorTechnicalContactMailID = By.id("technicalEmailId");
	public static By locatorTechnicalContactNumber = By.id("technicalPhoneNumber");
	public static By locatorStorePrimaryContactMailID = By.id("primaryEmailid");
	public static By locatorStorePrimaryContactNumber = By.id("primaryPhoneNum");
	public static By locatorStoreTechnicalContactName = By.id("techName");
	public static By locatorStoreTechnicalContactMailID = By.id("techEmailID");
	public static By locatorStoreTechnicalContactNumber = By.id("techPhoneNum");
	public static By locatorInstructionField = By.id("instructions");
	public static By locatorSaveButton = By.id("saveMerchant");
	public static By locatorSelectedPackageRadioBtn = By.xpath("//label[contains(@class,'label_radio')]");
	public static By locatorAddNewCompanyLink = By.id("addCompany");
	public static By locatorLimitedOfferingRadioBtn = By.id("addCompany");
	public static By locatorConfigManagementMenuOption = By.linkText("Config Management");
	public static By locatorStoreOptionUnderConfigManagementMenu = By.linkText("Stores");
	public static By locatorStoreHeaderPanel = By.cssSelector("header.panel-heading");
	public static By locatorStoreDetailsHeaderPanel = By
			.xpath(".//*[@id='storeSelect']/div/header[contains(text(),'Store Details')]");
	public static By locatorMerchantDropdownInStorePage = By.id("merchant");
	public static By locatorStoreDropdownInStorePage = By.id("stores1");
	public static By locatorStoreNickNameField = By.id("nickName");
	public static By locatorStoreAddress1Field = By.id("address1");
	public static By locatorStoreAddress2Field = By.id("address2");
	public static By locatorStateDropdown = By.id("state");
	public static By locatorPostalCodeField = By.id("postalCode");
	public static By locatorStorePhoneField = By.id("phoneNumbers");
	public static By locatorAvailHrsField = By.id("availHrsWeek");
	public static By locatorAvailHrsSatField = By.id("availHrsSat");
	public static By locatorAvailHrsSunField = By.id("availHrsSun");
	public static By locatorSaveStoreButton = By.id("saveStore");
	public static By locatorDeleteStoreButton = By.id("deleteStore");
	public static By locatorLogonPageHeader = By.xpath("//a[@class='logo']");
	public static By locatorHomeButton = By.id("homeButton");
	public static By locatorLogonPageBackButton = By.id("backButton");
	public static By locatorCompanyNameText = By.xpath("//label[contains(text(),'Name of the Company')]/span");
	public static By locatorNickNameText = By.xpath("//label[contains(text(),'Nick Name')]/span");
	public static By locatorSelectedPackageText = By.xpath("//label[contains(text(),'Selected Package')]/span");
	public static By locatorPrimaryContactNameText = By.xpath("//label[contains(text(),'Primary Contact Name')]/span");
	public static By locatorPrimaryMailIDText = By.xpath("//label[contains(text(),'Primary Contact Email-ID')]/span");
	public static By locatorPrimaryContactNumberText = By
			.xpath("//label[contains(text(),'Primary Contact Number')]/span");
	public static By locatorTechnicalContactNameText = By
			.xpath("//label[contains(text(),'Technical Contact Name')]/span");
	public static By locatorTechnicalMailIDText = By
			.xpath("//label[contains(text(),'Technical Contact Email-ID')]/span");
	public static By locatorTechnicalContactNumberText = By
			.xpath("//label[contains(text(),'Technical Contact Number')]/span");
	public static By locatorEmployeeOptionUnderConfigManagementMenu = By.linkText("Employees");
	public static By locatorNewEmployeeLink = By.id("newEmployee");
	public static By locatorEmployeeHeaderPanel = By.cssSelector("span.portalIcon");
	public static By locatorStoreDropdownInEmployeePage = By.id("storeSelect");
	public static By locatorUserNameFieldInEmployeePage = By.id("userNameSearch");
	public static By locatorAddEmployeePopup = By.xpath("//div[@class='modal-dialog']");
	public static By locatorEmployeeFirstNameInAddEmployeePopup = By.id("firstName");
	public static By locatorEmployeeLastNameInAddEmployeePopup = By.id("lastName");
	public static By locatorEmployeePreferredNameInAddEmployeePopup = By.id("preferredName");
	public static By locatorCustomEmployeeIDInAddEmployeePopup = By.id("customEmpId");
	public static By locatorProfileIDInAddEmployeePopup = By.id("profileId");
	public static By locatorManagerAppChkboxInAddEmployeePopup = By.id("mapp");
	public static By locatorManagerAppMailFieldInAddEmployeePopup = By.id("mAppEmail");
	public static By locatorStoresTab = By.xpath("//a[@data-toggle='tab'][contains(text(),'Stores')]");
	public static By locatorGroupsTab = By.xpath("//a[@data-toggle='tab'][contains(text(),'Groups')]");
	public static By locatorCancelButton = By.xpath("//button[text()='Cancel']");
	public static By locatorSaveEmployeeButton = By.id("saveEmployee");
	public static By locatorLoadingText = By.xpath("//div[contains(text(),'Working... Please wait.')]");
	public static By locatorGrammarLinkUnderConfigManagementMenu = By.xpath("//*[@id='nav-accordion']//a[@href='#groups']");
	public static By locatorEmployeeGrammarLink = By.linkText("Employee Grammar");
	public static By locatorEmployeeGrammarHeaderPanel = By.cssSelector("span.portalIcon");
	public static By locatorEmployeeIDColumnLinkInEmployeeGrammarPage = By.linkText("Emp Id");
	public static By locatorFirstNameColumnLinkInEmployeeGrammarPage = By.linkText("First Name");
	public static By locatorPreferredNameColumnLinkInEmployeeGrammarPage = By.linkText("Preferred Name");
	public static By locatorLastNameColumnLinkInEmployeeGrammarPage = By.linkText("Last Name");
	public static By locatorFirstNameAliasColumnLinkInEmployeeGrammarPage = By
			.cssSelector("th.renderable.firstNameAlias");
	public static By locatorLastNameAliasColumnLinkInEmployeeGrammarPage = By
			.cssSelector("th.renderable.lastNameAlias");
	public static By locatorGroupsOptionUnderConfigManagementMenu = By.linkText("Groups");
	public static By locatorGroupHeaderPanel = By.cssSelector("header.panel-heading");
	public static By locatorGroupDropdownInGroupsPage = By.id("groupList");
	public static By locatorAddNewGroupLinkInGroupsPage = By.id("addNewGroup");
	public static By locatorGroupNameFieldInGroupsPage = By.id("name");
	public static By locatorGroupUsersTabInGroupsPage = By.linkText("Group Users");
	public static By locatorVoiceTabInGroupsPage = By.id("voiceTabRecGrps");
	public static By locatorAddNewUserLinkInGroupsPage = By.id("addNewUser");
	public static By locatorDeleteUserLinkInGroupsPage = By.id("deleteUser");
	public static By locatorGroupDeleteBtnInGroupsPage = By.id("groupDelete");
	public static By locatorGroupSaveBtnInGroupsPage = By.id("saveButton");
	public static By locatorSalesOption = By
			.linkText("Sales Update");
	public static By locatorSalesUpdateHeader = By
			.xpath(".//*[@id='main-content']/section/header[contains(text(), 'Sales Update')]");
	public static By locatorAdminButtonInSales = By.id("user-dropdown");
	public static By locatorCompanyName = By.id("merchant");
	public static By locatorStoreName = By.id("storeSelect");
	public static By locatorHuddle = By
			.linkText("Huddle");
	public static By locatorHuddlegroupName = By.id("groupList");
	public static By locatorAlarmDefinitionOption = By
			.xpath(".//*[@id='nav-accordion']/li/ul/li/a[contains(text(),'Alarm Definition')]");
	public static By locatorAlarmDefinitionHeader = By
			.xpath(".//*[@id='main-content']/section/header[contains(text(), 'Alarm Definition')]");
	public static By locatorAdminButtonInAlarmDefinition = By.id("user-dropdown");
	public static By locatorID = By.xpath("//a[contains(text(),'ID')]");
	public static By locatorAlarmSubscriptionOption = By
			.xpath(".//*[@id='nav-accordion']/li/ul/li/a[contains(text(),'Subscription')]");
	public static By locatorCDMRole = By.xpath(".//*[@id='backLoginPasswordRoles']/div[contains(@class,'cdmLogin')]");
	public static By locatorCDMButton = By.id("go-to-new-cdm");
	public static By locatorAAAMenu = By.xpath(".//*[@id='nav-accordion']/li/a/span[contains(text(),'AAA')]");
	public static By locatorManagerAppOption = By.xpath(".//*[@id='nav-accordion']//a[@href='#manager-app']");
	public static By locatorManagerAppHeader = By.xpath("//*[@id='main-content']//section/header/span[contains(text(),'Manager App')]");
	public static By locatorManagerAppAdminButton = By.id("user-dropdown");
	public static By locatorUserNameSearchInManagerApp = By.id("userMappNameSearch");
	public static By locatorRoleDropDn = By.id("roleSelect");
	public static By locatorAddNewUser = By.id("newManagerEmp");
	public static By locatorLocationOption = By.xpath("//*[@id='nav-accordion']//a[@href='#bnf-location']");
	public static By locatorLocationsHeader = By.xpath("//*[@id='main-content']/section/header[contains(.,'Locations')]");
	public static By locatorCreateLocation = By.id("create-location");
	public static By locatorStoreSaveConfirmation = By.xpath("//*[@class='bootbox-body'][contains(text(),'Store Saved Successfully')]");
	public static By locatorOkButton = By.xpath("//*[contains(text(),'OK')]");
	public static By locatorGrammer = By.xpath("//*[@id='nav-accordion']//span[contains(text(),'Grammars')]");
	public static By locatorNuanceGrammer = By.xpath("//*[@id='nav-accordion']//a[@href='#nuance-commands-config']");
	public static By locatorNuanceGrammerHeader = By.xpath("//*[@id='main-content']/section/header[contains(.,' Nuance Commands Configuration')]");
	public static By locatorNuanceId = By.xpath("//*[@id='nuance-config-grid']//th[contains(text(),'Id')]"); 
	public static By locatorNuanceCommand = By.xpath("//*[@id='nuance-config-grid']//th[contains(text(),'Command')]");
	public static By locatorNuanceTagout = By.xpath("//*[@id='nuance-config-grid']//th[contains(text(),'Tagout')]");
	public static By locatorNuanceAliases = By.xpath("//*[@id='nuance-config-grid']//th[contains(text(),'Aliases')]");
	public static By locatorNuanceTargets = By.xpath("//*[@id='nuance-config-grid']//th[contains(text(),'Targets')]");
	public static By locatorNuanceSave = By.xpath("//*[@id='nuance-config-grid']//th[contains(text(),'Save')]");
	public static By locatorGroupListContainer = By.id("groupListContainer");
	public static By locatorGroupManagementMenu = By
			.xpath(".//*[@id='nav-accordion']//span[contains(text(),'Group Management')]");
	
	public static By locatorTheatroStoreManagerRole = By.xpath("//*[@id='logoBottomText1'][contains(text(),'Theatro Store Manager')]");
	public static By locatorRBLButton = By.xpath("//*[@id='backLoginPasswordRoles']//button[@class='tsmGo empbtns']");
	public static By locatorReminderTimeButton = By.id("addReminder");
	public static By locatorSalesUpdateSuccessSaveMessage = By.xpath("//*[@class='btn btn-primary'][contains(text(),'OK')]");
	public static By locatorHuddleSuccessSaveMessage = By.xpath("//*[@class='bootbox-body'][contains(text(),'Huddle saved successfully.')]");
	public static By locatorHuddleName = By.id("name");
	public static By locatorZonesOption = By.xpath("//*[@id='nav-accordion']//a[@href='#bnf-zone']");
	public static By locatorZonesHeader = By.xpath("//*[@id='main-content']/section/header[contains(.,'Zone')]");
	public static By locatorCreateZone = By.id("create-zone");
	public static By locatorCombinedLocationsOption = By.xpath("//*[@id='nav-accordion']//a[@href='#bnf-combined-location']");
	public static By locatorCombinedLocationsHeader = By.xpath("//*[@id='main-content']/section/header[contains(.,'Combined Locations')]");
	public static By locatorCreateCombinedLocationButton = By.id("create-location");
	
	
	/* ................. Names of locators for the test ............... */
	public static String nameLoginPageUserIdInput = "'User ID Input box' in Theatro login page";
	public static String nameLoginPagePasswordInput = "'Password Input box' in Theatro login page";
	public static String nameLoginPageSignInBtn = "'Sign In button' in Theatro login page";
	public static String nameSideBar = "'Sidebar' in Theatro home page";
	public static String nameEmployeeHeader = "'Employee Header' in Theatro home page";
	public static String nameCompanyDropdown = "'Company dropdown' in Employee page";
	public static String nameStoreDropdown = "'Store dropdown' in Employee page";
	public static String nameUserNameInput = "'Username input field' in Employee page";
	public static String nameAddNewEmployeeLink = "'Add new employee link' in Employee page";
	public static String nameForgotPassword = "'Forgot password link' in Theatro login page";
	public static String nameConfigManagementMenu = "'Config Management Menu' in Theatro home page";
	public static String nameFaultManagementMenu = "'Fault Management Menu' in Theatro home page";
	public static String nameProfileOption = "'Profile option' under Config Management Menu";
	public static String nameProfileHeader = "'Profile Header' in Profile page";
	public static String nameAdminButtonInProfile = "'Admin Button ' in Profile page";
	public static String nameCompanyDropDn = "'Company Drop down' in Profile/Sales/ManagerApp page";
	public static String nameStoreDropDn = "'Store Drop down' in Profile/Sales page";
	public static String nameProfileUser = "'Profile User Button' in Profile page";
	public static String nameProfileUserList = "'Profile User List Button' in Profile page";
	public static String nameProfileDropDn = "'Profile Drop down' in Profile page";
	public static String nameAddNewProfile = "'Add new Profile' in Profile page";
	public static String nameNameButtonInProfile = "'Name Button in Profile' in Profile page";
	public static String nameNameInProfile = "'Name in Profile' in Profile page";
	public static String nameCommand = "'Command button' in Profile page";
	public static String nameCommandList = "'Command in Profile' in Profile page";
	public static String nameAddProfileUser = "'Add Profile User in Profile' in Profile page";
	public static String nameDeleteProfileUser = "'Delete Profile User in Profile' in Profile page";
	public static String nameSaveProfile = "'Save Button in' in Profile page";
	public static String nameOnboardingMenuOption = "'Onboarding Menu Option' in Theatro home page";
	public static String nameCompanyOptionUnderOnboardingMenu = "'Company Option' under Onboarding Menu";
	public static String nameCompanyDropdownInCompanyPage = "'Company dropdown' in Company page";
	public static String nameCompanyDetailsHeader = "'Company deatils header' in Company page";
	public static String nameNameField = "'Name input field'";
	public static String nameNickNameField = "'Nick name input field' in Company page";
	public static String nameAddressLine1Field = "'Address line1 input field' in Company page";
	public static String nameAddressLine2Field = "'Address line2 input field' in Company page";
	public static String nameCityField = "'City input field' in Company page";
	public static String nameZipCodeField = "'Zip code input field' in Company page";
	public static String nameCountryDropdown = "'Country dropdown' in Company page";
	public static String namePrimaryContactName = "'Primary Contact name input field' in Company page";
	public static String namePrimaryContactMailID = "'Primary Contact mail ID input field' in Company page";
	public static String namePrimaryContactNumber = "'Primary Contact number input field' in Company page";
	public static String nameTechnicalContactName = "'Technical Contact name input field' in Company page";
	public static String nameTechnicalContactMailID = "'Technical Contact mail ID input field' in Company page";
	public static String nameTechnicalContactNumber = "'Technical Contact number input field' in Company page";
	public static String nameInstructionField = "'Instruction field' in Company page";
	public static String nameSaveButton = "'Save button' in Company page";
	public static String nameSelectedPackageRadioBtn = "'Selected package radio button' in Company page";
	public static String nameAddNewCompanyLink = "'Add new company link' in Company page";
	public static String nameConfigManagementMenuOption = "'Config management menu option' in Theatro home page";
	public static String nameStoreOptionUnderConfigManagementMenu = "'Store option' under config management menu";
	public static String nameMerchantDropdownInStorePage = "'Merchant dropdown' in Store page";
	public static String nameStoreDropdownInStorePage = "'Store Dropdown' in Store page";
	public static String nameStoreAddress1Field = "'Store Address1 field' in Store page";
	public static String nameStoreAddress2Field = "'Store Address2 field' in Store page";
	public static String nameStateDropdown = "'State dropdown' in Store page";
	public static String namePostalCodeField = "'Postal code field' in Store page";
	public static String nameStorePhoneField = "'Store phone field' in Store page";
	public static String nameAvailHrsField = "'Avail Hrs field' in Store page";
	public static String nameAvailHrsSatField = "'Avail Hrs saturday field' in Store page";
	public static String nameAvailHrsSunField = "'Avail Hrs sunday field' in Store page";
	public static String nameSaveStoreButton = "'Save store button' in Store page";
	public static String nameDeleteStoreButton = "'Delete store button' in Store page";
	public static String nameLogonPageHeader = "'Page header' in logon tool";
	public static String nameHomeButton = "'Home button' in logon tool";
	public static String nameLogonPageBackButton = "'Back button' in logon tool";
	public static String nameEmployeeOptionUnderConfigManagementMenu = "'Employee option' under Config management Menu";
	public static String nameUserNameFieldInEmployeePage = "'User name field' in employees page";
	public static String nameNewEmployeeLink = "'Add new employee link' in employees page";
	public static String nameAddEmployeePopup = "'Add new employee popup' in employees page";
	public static String nameEmployeeFirstNameInAddEmployeePopup = "'Employee first name field' in add employees popup";
	public static String nameEmployeeLastNameInAddEmployeePopup = "'Employee last name field' in add employees popup";
	public static String nameEmployeePreferredNameInAddEmployeePopup = "'Employee preferred name field' in add employees popup";
	public static String nameCustomEmployeeIDInAddEmployeePopup = "'Custom Employee ID field' in add employees popup";
	public static String nameProfileIDInAddEmployeePopup = "'Profile ID field' in add employees popup";
	public static String nameManagerAppChkboxInAddEmployeePopup = "'Manager App checkbox' in add employees popup";
	public static String nameManagerAppMailFieldInAddEmployeePopup = "'Manager App checkbox' in add employees popup";
	public static String nameStoresTab = "'Stores Tab' in add employees popup";
	public static String nameGroupsTab = "'Stores Tab' in add employees popup";
	public static String nameCancelButton = "'Cancel Button' in add employees popup";
	public static String nameSaveEmployeeButton = "'Save Employee Button' in add employees popup";
	public static String nameLoadingText = "'Loading Text' in Theatro Webpage";
	public static String nameGrammarLinkUnderConfigManagementMenu = "'Grammar Link' under Config Management Menu";
	public static String nameEmployeeGrammarLink = "'Employee Grammar Link' under Grammar submenu";
	public static String nameEmployeeIDColumnLinkInEmployeeGrammarPage = "'Employee ID column link' in employee Grammar page";
	public static String nameFirstNameColumnLinkInEmployeeGrammarPage = "'First name column link' in employee Grammar page";
	public static String namePreferredNameColumnLinkInEmployeeGrammarPage = "'Preferred name column link' in employee Grammar page";
	public static String nameLastNameColumnLinkInEmployeeGrammarPage = "'Last name column link' in employee Grammar page";
	public static String nameFirstNameAliasColumnLinkInEmployeeGrammarPage = "'First name alias column link' in employee Grammar page";
	public static String nameLastNameAliasColumnLinkInEmployeeGrammarPage = "'Last name alias column link' in employee Grammar page";
	public static String nameGroupsOptionUnderConfigManagementMenu = "'Groups option' under Config Management Menu";
	public static String nameGroupHeaderPanel = "'Header Panel' in Groups page";
	public static String nameGroupDropdownInGroupsPage = "'Group dropdown' in Groups page";
	public static String nameAddNewGroupLinkInGroupsPage = "'Add new group Link' in Groups page";
	public static String nameGroupNameFieldInGroupsPage = "'Group name field' in Groups page";
	public static String nameGroupUsersTabInGroupsPage = "'Group Users tab' in Groups page";
	public static String nameVoiceTabInGroupsPage = "'Voice tab' in Groups page";
	public static String nameAddNewUserLinkInGroupsPage = "'Add new user link' in Groups page";
	public static String nameDeleteUserLinkInGroupsPage = "'Remove user from group link' in Groups page";
	public static String nameGroupDeleteBtnInGroupsPage = "'Remove group button' in Groups page";
	public static String nameGroupSaveBtnInGroupsPage = "'Save group button' in Groups page";
	public static String nameSalesOption = "'Sales Update option' under Config Management Menu";
	public static String nameSalesUpdate = "'Sales Update button' in Sales Update page";
	public static String nameAdminButtonInSales = "'Admin Button ' in Sales Update page";
	public static String nameHuddle = "'Huddle Page' under Config Management Menu";
	public static String nameCompanyName = "'Company Name drop down' in Sales/huddle/Alarm Definition  page";
	public static String nameStoreName = "'Store Name drop down' in Sales/huddle/Alarm Definition page";
	public static String nameHuddlegroupName = "'Huddle group  Name drop down' in huddle page";
	public static String nameAlarmDefinition = "'Alarm Definistion' under Fault Management Menu";
	public static String nameAlarmDefinitionHeader = "'Alarm Definition Header' in Profile page";
	public static String nameAdminButtonInAlarmDefinition = "'Admin Button ' in Alarm Definition page";
	public static String nameAlarmSubscription = "'Alarm Subscription' under Fault Management Menu";
	public static String nameCDMRole = "'CDM Role tag' in role based login page";
	public static String nameCDMButton = "'Go to CDM button' in role based login page";
	public static String nameAAAMenu = "'AAA Menu' in Theatro home page";
	public static String nameManagerAppOption = "'Manager App Option' under AAA menu";
	public static String nameManagerAppHeader = "'Manager App Header' in Manager App Page";
	public static String nameManagerAppAdminButton = "'Admin Button' in Manager App Page";
	public static String nameUserNameSearchInManagerApp = "'User Name Search' in Manager App page";
	public static String nameRoleDropDn = "'Role Drop Down' in Manager App Page";
	public static String nameAddNewUser = "'Add New User' in Manager App Page";
	public static String nameLocationOption = "'Locations Options' under config management menu";
	public static String nameLocationsHeader = "'Locations Header' in Locations Page";
	public static String nameCreateLocation = "'Create Location button' in Locations Page";
	public static String nameOkButton = "'Ok button' present in save page";
	public static String nameGrammer = "'Grammars menu' present in config management menu";
	public static String nameNuanceGrammer = "'Nuance Grammer' present in Grammars menu";
	public static String nameNuanceGrammerHeader = "'Nuance Grammer Header' present in Nuance Grammer page";
	public static String nameNuanceId = "'Id' is present in Nuance Grammer page";
	public static String nameNuanceCommand = "'Command' is present in Nuance Grammer page";
	public static String nameNuanceTagout = "'Tagout' is present in Nuance Grammer Page";
	public static String nameNuanceAliases = "'Aliases' is present in Nuance Grammer page";
	public static String nameNuanceTargets = "'Targets' is present in Nuance Grammer page"; 
	public static String nameNuanceSave = "'Save' is present in Nuance Grammer page";
	public static String nameGroupManagementMenu = "'Group Management Menu' in Theatro home page";
	public static String nameTheatroStoreManagerRole = "'Theatro Store Manager Role tag' in role based login page";
	public static String nameRBLButton = "'tsmGo empbtns' button in role based login page";
	public static String nameReminderTimeButton = "Reminder time button is present in Sales Update";
	public static String nameSalesUpdateSuccessSaveMessage = "Sales Update Success message in sales update page";
	public static String nameHuddleSuccessSaveMessage = "Sales Update Success message in Huddle page";
	public static String nameHuddleName = "Huddle group name field is present in Huddle page";
	public static String nameZonesOption = "'Zones Option' under config management menu";
	public static String nameZonesHeader = "Zone Header in Zones page";
	public static String nameCreateZone = "Create Zone button is present in Zones page";
	public static String nameCombinedLocationsOption = "Combined Locations Option is present under config management menu";
	public static String nameCombinedLocationsHeader = "Combined Locations Header is present in Combined Locations page";
	public static String nameCreateCombinedLocationButton = "Create Combined Location button is present in Combined Locations page";
	
	// login to Theatro Central web portal
	public static String loginToTheatroCentral(String userType) throws FileNotFoundException, InterruptedException {

		System.out.println("Login to theatro web portal ...");

		String userName = null;
		String password = null;
		/* har = server.getHar(); */
		try {

			userName = testData.getCellData(userType, "UserName", 1);
			password = testData.getCellData(userType, "Password", 1);

			APPLICATION_LOGS.debug("Successfully Retrieved data from Xls File :-  Username : " + userName
					+ " and Password : " + password);

		}

		catch (Throwable fetchExcelDataError) {

			APPLICATION_LOGS.debug("Error while retrieving data from xls file" + fetchExcelDataError.getMessage());
			return "Error while retrieving data from xls file" + fetchExcelDataError.getMessage();

		}
		
		// clear User Id field
					methodReturnResult = FunctionLibrary.clearField(locatorLoginPageUserIdInput, nameLoginPageUserIdInput);
					if (methodReturnResult.contains("Fail"))
						return methodReturnResult;

					// Input username in User Id field
					methodReturnResult = FunctionLibrary.input(locatorLoginPageUserIdInput, nameLoginPageUserIdInput, userName);
					if (methodReturnResult.contains("Fail"))
						return methodReturnResult;

					// clear User Id field
					methodReturnResult = FunctionLibrary.clearField(locatorLoginPagePasswordInput, nameLoginPagePasswordInput);
					if (methodReturnResult.contains("Fail"))
						return methodReturnResult;

					// Input password in Password field
					methodReturnResult = FunctionLibrary.input(locatorLoginPagePasswordInput, nameLoginPagePasswordInput, password);
					if (methodReturnResult.contains("Fail"))
						return methodReturnResult;

					// Click on Sign In button
					methodReturnResult = FunctionLibrary.clickLink(locatorLoginPageSignInBtn, nameLoginPageSignInBtn);
					if (methodReturnResult.contains("Fail"))
						return methodReturnResult;

		if(userType=="login") {

			/*
			 * List<HarEntry> results = har.getLog().getEntries();
			 * System.out.println("Har File have " + results.size() + " Requests");
			 * for (HarEntry result : results) { for (int i = 0; i <
			 * result.getRequest().getQueryString().size(); i++) {
			 * System.out.println("<br>action = " +
			 * result.getRequest().getQueryString().get(i).getName());
			 * System.out.println("<br>action = " +
			 * result.getRequest().getQueryString().get(i).getValue()); } }
			 */

			/*
			 * // get the HAR data har = server.getHar(); System.out.println("HAR: "
			 * + har); fos= new FileOutputStream(System.getProperty("user.dir") +
			 * "/Report/sample.har");
			 */
			

			// wait for page load
			FunctionLibrary.waitForPageToLoad();
			

			// wait for element to disappear
			FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
			

			// wait for page load
			FunctionLibrary.waitForPageToLoad();

			
			// Verify login was successful
			if (!FunctionLibrary.isElementDisplayed(locatorSideBar, nameSideBar)) {
				return "Fail - Side bar is not displayed after logging into Theatro site";
			}

			// Verify the presence of employee header
			methodReturnResult = FunctionLibrary.assertText(nameEmployeeHeader, driver.findElement(locatorEmployeeHeader).getText(), Constants.employeeHeaderText);
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}

			// Verify the presence of company dropdown
			if (!FunctionLibrary.isElementDisplayed(locatorCompanyDropdown, nameCompanyDropdown)) {
				return "Fail - Company dropdown is not displyed after logging into Theatro site in employees page";
			}

			// Verify the presence of store dropdown
			if (!FunctionLibrary.isElementDisplayed(locatorStoreDropdown, nameStoreDropdown)) {
				return "Fail - Store dropdown is not displyed after logging into Theatro site in employees page";
			}

			// Verify the presence of username field
		/*	if (!FunctionLibrary.isElementDisplayed(locatorUserNameInput, nameUserNameInput)) {
				return "Fail - User name input field is not displyed after logging into Theatro site in employees page";
			} */

			// Verify the presence of Add new employee link
			if (!FunctionLibrary.isElementDisplayed(locatorAddNewEmployeeLink, nameAddNewEmployeeLink)) {
				return "Fail - Add new employee link is not displyed after logging into Theatro site in employees page";
			} 

			APPLICATION_LOGS.debug("Logged into Theatro Central");
		}
		
		
		
		if(userType == "CDM") {
			
			//Verify the presence of CDM Role element
			if(!FunctionLibrary.isElementDisplayed(locatorCDMRole, nameCDMRole)) {
				return "Fail - CDM Role tag doesn't exist in Role based login page";
			}
			
			//Verify the presence of Go to CDM Button
			if(!FunctionLibrary.isElementDisplayed(locatorCDMButton, nameCDMButton)) {
				return "Fail - Go to CDM Button doesn't exist in Role based login page";
			}
			
			APPLICATION_LOGS.debug("Pass : Logged into CDM ");
		}
		
		
		
		if(userType == "RoleBasedLogin") {
			//verify the presence of Theatro store manager
			if(!FunctionLibrary.isElementDisplayed(locatorTheatroStoreManagerRole, nameTheatroStoreManagerRole)) {
				return "Fail - Theatro Store Manager role tag doesn't exist in Role Based login page";
			}
			
			//Verify the presence of Theatro Store Manager button
			if(!FunctionLibrary.isElementDisplayed(locatorRBLButton, nameRBLButton)) {
				return "Fail - Theatro Store Manager Button does't exist in role based login page";
			}
			
			
		}
		
		return "Pass : Navigate to TheatroCentral";
	}
	
	
	// Create a browser instance and navigate to the Theatro Central
	public static String navigateToTheatroCentral() throws Exception {

		System.out.println("Creating a browser instance and navigating to the test site ...");

		if (wbdv == null) {

			try {
				if (CONFIG.getProperty("test_browser").toLowerCase().contains("chrome")) {

					System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/chromedriver.exe");
					
					ChromeOptions options = new ChromeOptions(); 
					options.addArguments("allow-file-access-from-files"); 
					options.addArguments("use-fake-device-for-media-stream");
					 options.addArguments("use-fake-ui-for-media-stream"); 


					
					
					//dc = DesiredCapabilities.chrome();
					wbdv = new ChromeDriver(options);
					driver = new EventFiringWebDriver(wbdv);

					// Delete all browser cookies
					driver.manage().deleteAllCookies();

				}

				else if (CONFIG.getProperty("test_browser").toLowerCase().contains("firefox")
						|| CONFIG.getProperty("test_browser").toLowerCase().contains("ff")) {

					ProfilesIni allProfiles = new ProfilesIni();
					FirefoxProfile profile = allProfiles.getProfile("default");
					
					  profile.setPreference("network.proxy.http", "localhost");
					  profile.setPreference("network.proxy.http_port", 4446);
					  profile.setPreference("network.proxy.ssl", "localhost");
					  profile.setPreference("network.proxy.ssl_port", 4446);
					  profile.setPreference("network.proxy.type", 1);
					  
					  profile.setAcceptUntrustedCertificates(true);
					  profile.setAssumeUntrustedCertificateIssuer(true);
					  
					  DesiredCapabilities capabilities = new
								  DesiredCapabilities();
					  capabilities.setCapability(FirefoxDriver.PROFILE,
					  profile); profile.setAcceptUntrustedCertificates(true);
					  profile.setAssumeUntrustedCertificateIssuer(false);
					 

					profile.setPreference("network.proxy.type", ProxyType.AUTODETECT.ordinal());

					profile.setAcceptUntrustedCertificates(true);
					profile.setAssumeUntrustedCertificateIssuer(true);

					/*
					 * // configure it as a desired capability // get the
					 * Selenium proxy object Proxy proxy =
					 * server.seleniumProxy();
					 * 
					 * DesiredCapabilities capabilities = new
					 * DesiredCapabilities();
					 * capabilities.setCapability(CapabilityType.PROXY, proxy);
					 */
					
                    
					wbdv = new FirefoxDriver();
					driver = new EventFiringWebDriver(wbdv);

					/*
					 * server.newHar(CONFIG.getProperty("theatroCentralURL"));
					 */

					// Delete all browser cookies
					driver.manage().deleteAllCookies();
				}

			}

			catch (Throwable initBrowserException) {

				APPLICATION_LOGS
						.debug("Error came while creating a browser instance : " + initBrowserException.getMessage());

				return "Fail : Error came while creating a browser instance : " + initBrowserException.getMessage();

			}

		}

		APPLICATION_LOGS.debug("Created browser instance successfully");

		try {

			// Implicitly wait for 30 seconds for browser to open
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			// Navigate to Curaspan application
			driver.navigate().to(CONFIG.getProperty("theatroCentralURL"));

			// Maximize browser window
			APPLICATION_LOGS.debug("Maximizing Browser window...");
			driver.manage().window().maximize();
			APPLICATION_LOGS.debug("Browser window is maximized");
		}

		catch (Throwable navigationError) {

			APPLICATION_LOGS
					.debug("Error came while navigating to the Theatro Central site : " + navigationError.getMessage());

		}

		// Wait for elemnts to load
		FunctionLibrary.waitForElementToLoad(locatorLoginPageUserIdInput);

		// Verify elements in login page is displyaed or not
		if (!FunctionLibrary.isElementDisplayed(locatorLoginPageUserIdInput, nameLoginPageUserIdInput)) {
			return "Fail - Login page doesnot contain User ID input box";
		}

		if (!FunctionLibrary.isElementDisplayed(locatorLoginPagePasswordInput, nameLoginPagePasswordInput)) {
			return "Fail - Login page doesnot contain Password input box";
		}

		if (!FunctionLibrary.isElementDisplayed(locatorLoginPageSignInBtn, nameLoginPageSignInBtn)) {
			return "Fail - Login page doesnot contain Sign In input box";
		}

		if (!FunctionLibrary.isElementDisplayed(locatorForgotPassword, nameForgotPassword)) {
			return "Fail - Login page doesnot contain Forgot password link";
		}

		APPLICATION_LOGS.debug("Navigated to Login page");

		return "Pass : Navigated to Login page";

	}

	// Navigate to profiles page present under config management menu
	public static String navigateToProfilePage() throws InterruptedException {

		APPLICATION_LOGS.debug("Navigate To Profiles page under Config management menu");
		
		// wait for page load
				FunctionLibrary.waitForPageToLoad();
						 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
				
		Thread.sleep(2000);
		//wait for element for to element
		FunctionLibrary.waitForElementToLoad(locatorConfigManagementMenu);

		// Click on Config Management Menu
		methodReturnResult = FunctionLibrary.clickLink(locatorConfigManagementMenu, nameConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorProfileOption);

		// Click on Profile option
		methodReturnResult = FunctionLibrary.clickLink(locatorProfileOption, nameProfileOption);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Check for the Profiles header in profile page

		if (!FunctionLibrary.isElementDisplayed(locatorProfileHeader, nameProfileHeader)) {
			return "Fail : Profile header is not displayed in Profile";
		}

		// Check for the Admin Button in profile page

		if (!FunctionLibrary.isElementDisplayed(locatorAdminButtonInProfile, nameAdminButtonInProfile)) {
			return "Fail : Admin button  is not displayed in Profile";
		}

		// Check for the company drop down in profile page

		if (!FunctionLibrary.isElementDisplayed(locatorCompanyDropDn, nameCompanyDropDn)) {
			return "Fail : Company drop down is not displayed in Profile";
		}

		// Check for store drop down in profile page
		if (!FunctionLibrary.isElementDisplayed(locatorStoreDropDn, nameStoreDropDn)) {
			return "Fail : Store drop down is not displayed in Profile";
		}

		// Check for Profile drop down in profile page
		/*if (!FunctionLibrary.isElementDisplayed(locatorProfileDropDn, nameProfileDropDn)) {
			return "Fail : Profile drop down is not displayed in Profile";
		}*/

		// Check for Add new profile
		if (!FunctionLibrary.isElementDisplayed(locatorAddNewProfile, nameAddNewProfile)) {
			return "Fail : Add new Profile is not displayed in Profile";
		}

		// Check for Profile User Button

		if (!FunctionLibrary.isElementDisplayed(locatorNameButtonInProfile, nameNameButtonInProfile)) {
			return "Fail : Profile Button is not displayed in Profile";
		}

		// Check for Profile Name

		if (!FunctionLibrary.isElementDisplayed(locatorNameProfile, nameNameInProfile)) {
			return "Fail : Name of Profile is not displayed in Profile";
		}

		// Check for Commands button in Profile page
		if (!FunctionLibrary.isElementDisplayed(locatorCommands, nameCommand)) {
			return "Fail : Name of Profile is not displayed in Profile";
		}

		// Check for Commands list in Profile page
		if (!FunctionLibrary.isElementDisplayed(locatorCommandList, nameCommandList)) {
			return "Fail : Name of Profile is not displayed in Profile";
		}

		// Check for Profile User in Profile page
		if (!FunctionLibrary.isElementDisplayed(locatorProfileUser, nameProfileUser)) {
			return "Fail : Name of Profile is not displayed in Profile";
		}

		// Check for Profile User List in Profile page
		if (!FunctionLibrary.isElementDisplayed(locatorProfileUserList, nameProfileUserList)) {
			return "Fail : Name of Profile is not displayed in Profile";
		}

		// Check for Add Profile User in Profile page
		if (!FunctionLibrary.isElementDisplayed(locatorAddProfileUser, nameAddProfileUser)) {
			return "Fail : Name of Profile is not displayed in Profile";
		}

		// Check for Delete Profile User in Profile page
		if (!FunctionLibrary.isElementDisplayed(locatorDeleteProfileUser, nameDeleteProfileUser)) {
			return "Fail : Name of Profile is not displayed in Profile";
		}

		// Check for Save Button in Profile page
		if (!FunctionLibrary.isElementDisplayed(locatorDeleteProfileUser, nameDeleteProfileUser)) {
			return "Fail : Name of Profile is not displayed in Profile";
		}

		return "Pass : Navigated to Profiles page present under Config management Menu";

	}

	// Navigate to company page present under onboarding menu
	public static String navigateToCompanyPage() throws InterruptedException {

		APPLICATION_LOGS.debug("Navigate To company page and verify all the elements");
		
		// wait for page load
		FunctionLibrary.waitForPageToLoad();
				 
       // wait for element to disappear
       FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
       Thread.sleep(2000);
       //wait for element for to element
       FunctionLibrary.waitForElementToLoad(locatorConfigManagementMenu);

		// Click on Onboarding menu
		methodReturnResult = FunctionLibrary.clickLink(locatorOnboardingMenuOption, nameOnboardingMenuOption);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Click on Company link
		methodReturnResult = FunctionLibrary.clickAndWait(locatorCompanyOptionUnderOnboardingMenu,
				nameCompanyOptionUnderOnboardingMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Wait for page to load
		FunctionLibrary.waitForElementToLoad(locatorCompanyDropdownInCompanyPage);

		// Verify whether page is navigated to company page
		// Verify panel header
		methodReturnResult = FunctionLibrary.assertText("Company Header",
				driver.findElement(locatorCompanyHeaderPanel).getText(), Constants.companyPageHeader);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Verify company dropdown is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorCompanyDropdownInCompanyPage,
				nameCompanyDropdownInCompanyPage)) {
			return "Fail : Company dropdown in company page is not displayed";
		}

		// Verify the presence of add new

		// Verify the presence of Company details header
		if (!FunctionLibrary.isElementDisplayed(locatorCompanyDetailsHeader, nameCompanyDetailsHeader)) {
			return "Fail : Comapny details header is not displayed in company page";
		}

		// Verify the presence of company name field
		if (!FunctionLibrary.isElementDisplayed(locatorNameField, nameNameField)) {
			return "Fail : Company name input field is not displayed in company page";
		}

		// Verify the presence of nick name field
		if (!FunctionLibrary.isElementDisplayed(locatorNickNameField, nameNickNameField)) {
			return "Fail : Nick name input field is not displayed in company page";
		}

		// Verify the presence of selected packages radio buttons
		if (!FunctionLibrary.isElementDisplayed(locatorSelectedPackageRadioBtn, nameSelectedPackageRadioBtn)) {
			return "Fail : Selected package radio buttons is not displayed in company page";
		}

		// Verify the presence of address line1 field
		if (!FunctionLibrary.isElementDisplayed(locatorAddressLine1Field, nameAddressLine1Field)) {
			return "Fail : Address line1 input field is not displayed in company page";
		}

		// Verify the presence of address line2 field
		if (!FunctionLibrary.isElementDisplayed(locatorAddressLine2Field, nameAddressLine2Field)) {
			return "Fail : Address line2 input field is not displayed in company page";
		}

		// Verify the presence of City input field
		if (!FunctionLibrary.isElementDisplayed(locatorCityField, nameCityField)) {
			return "Fail : City input field is not displayed in company page";
		}

		// Verify the presence of Zip code input field
		if (!FunctionLibrary.isElementDisplayed(locatorZipCodeField, nameZipCodeField)) {
			return "Fail : Zip input field is not displayed in company page";
		}

         
		// Verify the presence of Country dropdown
		if (!FunctionLibrary.isElementDisplayed(locatorCountryDropdown, nameCountryDropdown)) {
			return "Fail : Country dropdown is not displayed in company page";
		}
		
		Thread.sleep(2000);
		//WebElement element = driver.findElement(locatorPrimaryContactName);
		 ((JavascriptExecutor) driver)
         .executeScript("window.scrollTo(0, document.body.scrollHeight)");
        
		 Thread.sleep(5000);
		// check if the primary contact name is disabled
		if (!FunctionLibrary.isElementEnabled(locatorPrimaryContactName, namePrimaryContactName)) {
			return "Fail : Primary contact name is enabled in Company page";
		}

		// check if the primary contact mail ID is disabled
		if (!FunctionLibrary.isElementEnabled(locatorPrimaryContactMailID, namePrimaryContactMailID)) {
			return "Fail : Primary contact mail ID is enabled in Company page";
		}

		// check if the primary contact number is disabled
		if (!FunctionLibrary.isElementEnabled(locatorPrimaryContactNumber, namePrimaryContactNumber)) {
			return "Fail : Primary contact number is enabled in Company page";
		}

		// check if the technical name is disabled
		if (!FunctionLibrary.isElementEnabled(locatorTechnicalContactName, nameTechnicalContactName)) {
			return "Fail : Technical name is enabled in Company page";
		}

		// check if the technical name is disabled
		if (!FunctionLibrary.isElementEnabled(locatorTechnicalContactMailID, nameTechnicalContactMailID)) {
			return "Fail : Technical contact mail ID is enabled in Company page";
		}

		// check if the technical name is disabled
		if (!FunctionLibrary.isElementEnabled(locatorTechnicalContactNumber, nameTechnicalContactNumber)) {
			return "Fail : Technical contact number is enabled in Company page";
		}

		// check if the instruction field is present in company page
		if (!FunctionLibrary.isElementDisplayed(locatorInstructionField, nameInstructionField)) {
			return "Fail : Instruction field is not displayed in company page";
		}

		// Check if the save button is displayed in company page
		if (!FunctionLibrary.isElementDisplayed(locatorSaveButton, nameSaveButton)) {
			return "Fail : Save button is not displayed in company page";
		}

		return "Pass : Navigated to Company page present under Onboarding Menu";

	}

	// Navigate to add new company link
	public static String navigateToAddNewCompany() {

		APPLICATION_LOGS.debug("Navigate To Company page under Onboarding menu");

		// Click on Add new company link
		methodReturnResult = FunctionLibrary.clickLink(locatorAddNewCompanyLink, nameAddNewCompanyLink);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Verify the name field is mandatory
		if (!driver.findElement(locatorCompanyNameText).getAttribute("class").equals(Constants.fieldRequiredClass)) {
			return "Fail : Company name field is not mandatory";
		}

		// Verify the presence of company name field
		if (!FunctionLibrary.isElementDisplayed(locatorNameField, nameNameField)) {
			return "Fail : Company name input field is not displayed in company page";
		}

		// Verify place holder for company name field
		methodReturnResult = FunctionLibrary.assertText("Company name input field placeholder",
				driver.findElement(locatorNameField).getAttribute("placeholder"), Constants.companyNamePlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Verify the nick name field is mandatory
		if (!driver.findElement(locatorNickNameText).getAttribute("class").equals(Constants.fieldRequiredClass)) {
			return "Fail : Nick name field is not mandatory";
		}

		// Verify the presence of nick name field
		if (!FunctionLibrary.isElementDisplayed(locatorNickNameField, nameNickNameField)) {
			return "Fail : Nick name input field is not displayed in company page";
		}

		/*
		 * // Verify the limited offering radio button is checked
		 * if(!FunctionLibrary.isChecked(locatorLimitedOfferingRadioBtn,
		 * nameLimitedOfferingRadioBtn)){ return
		 * "Fail : Limited offering radio button is not checked for add new employee"
		 * ; }
		 */

		// Verify the Selected Package field is mandatory
		if (!driver.findElement(locatorSelectedPackageText).getAttribute("class")
				.equals(Constants.fieldRequiredClass)) {
			return "Fail : Selected Package field is not mandatory";
		}

		// Verify the presence of address line1 field
		if (!FunctionLibrary.isElementDisplayed(locatorAddressLine1Field, nameAddressLine1Field)) {
			return "Fail : Address line1 input field is not displayed in company page";
		}

		// Verify place holder for address1 field
		methodReturnResult = FunctionLibrary.assertText("Address input field placeholder",
				driver.findElement(locatorAddressLine1Field).getAttribute("placeholder"),
				Constants.address1PlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Verify the presence of address line2 field
		if (!FunctionLibrary.isElementDisplayed(locatorAddressLine2Field, nameAddressLine2Field)) {
			return "Fail : Address line2 input field is not displayed in company page";
		}

		// Verify place holder for address1 field
		methodReturnResult = FunctionLibrary.assertText("Address2 input field placeholder",
				driver.findElement(locatorAddressLine2Field).getAttribute("placeholder"),
				Constants.address2PlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Verify the presence of City input field
		if (!FunctionLibrary.isElementDisplayed(locatorCityField, nameCityField)) {
			return "Fail : City input field is not displayed in company page";
		}

		// Verify place holder for city field
		methodReturnResult = FunctionLibrary.assertText("City input field placeholder",
				driver.findElement(locatorCityField).getAttribute("placeholder"), Constants.cityPlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Verify the presence of Zip code input field
		if (!FunctionLibrary.isElementDisplayed(locatorZipCodeField, nameZipCodeField)) {
			return "Fail : Zip input field is not displayed in company page";
		}

		// Verify place holder for zip field
		methodReturnResult = FunctionLibrary.assertText("Zip input field placeholder",
				driver.findElement(locatorZipCodeField).getAttribute("placeholder"), Constants.zipPlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Verify the presence of Country dropdown
		if (!FunctionLibrary.isElementDisplayed(locatorCountryDropdown, nameCountryDropdown)) {
			return "Fail : Country dropdown is not displayed in company page";
		}

		// check if the primary contact name is disabled
		/*if (!FunctionLibrary.isElementEnabled(locatorPrimaryContactName, namePrimaryContactName)) {
			return "Fail : Primary contact name is disabled in Company page";
		}*/

		// Verify place holder for primary contact name field
		methodReturnResult = FunctionLibrary.assertText("Primary contact field input field placeholder",
				driver.findElement(locatorPrimaryContactName).getAttribute("placeholder"),
				Constants.primaryContactNamePlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// check if the primary contact mail ID is disabled
		if (!FunctionLibrary.isElementEnabled(locatorPrimaryContactMailID, namePrimaryContactMailID)) {
			return "Fail : Primary contact mail ID is disabled in Company page";
		}

		// Verify place holder for mail ID field
		methodReturnResult = FunctionLibrary.assertText("mail ID field input field placeholder",
				driver.findElement(locatorPrimaryContactMailID).getAttribute("placeholder"),
				Constants.primaryMailIDPlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// check if the primary contact number is disabled
		if (!FunctionLibrary.isElementEnabled(locatorPrimaryContactNumber, namePrimaryContactNumber)) {
			return "Fail : Primary contact number is disabled in Company page";
		}

		// Verify placeholder for primary contact number
		methodReturnResult = FunctionLibrary.assertText("Primary contact field input field placeholder",
				driver.findElement(locatorPrimaryContactNumber).getAttribute("placeholder"),
				Constants.primaryContactNumberPlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// check if the technical name is disabled
		if (!FunctionLibrary.isElementEnabled(locatorTechnicalContactName, nameTechnicalContactName)) {
			return "Fail : Technical name is disabled in Company page";
		}

		// Verify placeholder for technical name
		methodReturnResult = FunctionLibrary.assertText("Technical name input field placeholder",
				driver.findElement(locatorTechnicalContactName).getAttribute("placeholder"),
				Constants.technicalNamePlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// check if the technical mail ID is disabled
		if (!FunctionLibrary.isElementEnabled(locatorTechnicalContactMailID, nameTechnicalContactMailID)) {
			return "Fail : Technical contact mail ID is disabled in Company page";
		}

		// Verify placeholder for technical mail ID
		methodReturnResult = FunctionLibrary.assertText("Technical Mail ID field input field placeholder",
				driver.findElement(locatorTechnicalContactMailID).getAttribute("placeholder"),
				Constants.technicalMailIDPlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// check if the technical name is disabled
		if (!FunctionLibrary.isElementEnabled(locatorTechnicalContactNumber, nameTechnicalContactNumber)) {
			return "Fail : Technical contact number is disabled in Company page";
		}

		// Verify placeholder for technical contact number
		methodReturnResult = FunctionLibrary.assertText("Technical contact number field input field placeholder",
				driver.findElement(locatorTechnicalContactNumber).getAttribute("placeholder"),
				Constants.technicalNumberPlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// check if the instruction field is present in company page
		if (!FunctionLibrary.isElementDisplayed(locatorInstructionField, nameInstructionField)) {
			return "Fail : Instruction field is not displayed in company page";
		}

		// Verify placeholder for additional instructions
		methodReturnResult = FunctionLibrary.assertText("Additional instruction field input field placeholder",
				driver.findElement(locatorInstructionField).getAttribute("placeholder"),
				Constants.additionalInstructionPlaceholderText);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Check if the save button is displayed in company page
		if (!FunctionLibrary.isElementDisplayed(locatorSaveButton, nameSaveButton)) {
			return "Fail : Save button is not displayed in company page";
		}

		return "Pass : Navigated to add company options in company page";
	}

	// Navigate to store page present under config management menu
	public static String navigateToStoresPage() throws InterruptedException {

		APPLICATION_LOGS.debug("Navigate To store page and verify all the elements");
		
		// wait for page load
		FunctionLibrary.waitForPageToLoad();
				 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		Thread.sleep(2000);
		//wait for element for to element
		FunctionLibrary.waitForElementToLoad(locatorConfigManagementMenu);

		// Click on config management menu
		methodReturnResult = FunctionLibrary.clickLink(locatorConfigManagementMenu,
				nameConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Click on Store link
		methodReturnResult = FunctionLibrary.clickAndWait(locatorStoreOptionUnderConfigManagementMenu,
				nameStoreOptionUnderConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Wait for page to load
		FunctionLibrary.waitForElementToLoad(locatorNameField);

		// Verify whether page is navigated to store page
		// Verify panel header
		methodReturnResult = FunctionLibrary.assertText("Store Header",
				driver.findElement(locatorStoreHeaderPanel).getText(), Constants.storePageHeader);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Verify merchant dropdown is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorMerchantDropdownInStorePage, nameMerchantDropdownInStorePage)) {
			return "Fail : Mercahnt dropdown in store page is not displayed";
		}

		// Verify store dropdown is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorStoreDropdownInStorePage, nameStoreDropdownInStorePage)) {
			return "Fail : Store dropdown in store page is not displayed";
		}

		// Verify store details panel header
		if (!driver.findElement(locatorStoreDetailsHeaderPanel).getText().contains(Constants.storeDetailsPageHeader))
			return "Fail : Store Details header text doesnot match with expected text";

		// Verify the presence of company name field
		if (!FunctionLibrary.isElementDisplayed(locatorNameField, nameNameField)) {
			return "Fail : Company name input field is not displayed in Store page";
		}

		// Verify the presence of nick name field
		if (!FunctionLibrary.isElementDisplayed(locatorStoreNickNameField, nameNickNameField)) {
			return "Fail : Nick name input field is not displayed in Store page";
		}

		// Verify the presence of City input field
		if (!FunctionLibrary.isElementDisplayed(locatorCityField, nameCityField)) {
			return "Fail : City input field is not displayed in Store page";
		}

		// Verify the presence of State dropdown in the page
		if (!FunctionLibrary.isElementDisplayed(locatorStateDropdown, nameStateDropdown)) {
			return "Fail : State dropdown is not displayed in Store page";
		}

		// Verify the presence of postal code field
		if (!FunctionLibrary.isElementDisplayed(locatorPostalCodeField, namePostalCodeField)) {
			return "Fail : postal code field is not displayed in Store page";
		}

		// Verify the presence of Country dropdown
		if (!FunctionLibrary.isElementDisplayed(locatorCountryDropdown, nameCountryDropdown)) {
			return "Fail : Country dropdown is not displayed in Store page";
		}

		// Verify the presence of address line1 field
		if (!FunctionLibrary.isElementDisplayed(locatorStoreAddress1Field, nameStoreAddress1Field)) {
			return "Fail : Address line1 input field is not displayed in Store page";
		}

		// Verify the presence of address line2 field
		if (!FunctionLibrary.isElementDisplayed(locatorStoreAddress2Field, nameStoreAddress2Field)) {
			return "Fail : Address line2 input field is not displayed in Store page";
		}

		// Verify the presence of store phone field
		if (!FunctionLibrary.isElementDisplayed(locatorStorePhoneField, nameStorePhoneField)) {
			return "Fail : Store phone input field is not displayed in store page";
		}
        
		Thread.sleep(2000);
		// check if the primary contact name is disabled
		/*if (FunctionLibrary.isElementEnabled(locatorPrimaryContactName, namePrimaryContactName)) {
			return "Fail : Primary contact name is enabled in Store page";
		}

		// check if the primary contact mail ID is disabled
		if (FunctionLibrary.isElementEnabled(locatorStorePrimaryContactMailID, namePrimaryContactMailID)) {
			return "Fail : Primary contact mail ID is enabled in Store page";
		}

		// check if the primary contact number is disabled
		if (FunctionLibrary.isElementEnabled(locatorStorePrimaryContactNumber, namePrimaryContactNumber)) {
			return "Fail : Primary contact number is enabled in Store page";
		}

		// check if the technical name is disabled
		if (FunctionLibrary.isElementEnabled(locatorStoreTechnicalContactName, nameTechnicalContactName)) {
			return "Fail : Technical name is enabled in Store page";
		}

		// check if the technical name is disabled
		if (FunctionLibrary.isElementEnabled(locatorStoreTechnicalContactMailID, nameTechnicalContactMailID)) {
			return "Fail : Technical contact mail ID is enabled in Store page";
		}

		// check if the technical name is disabled
		if (FunctionLibrary.isElementEnabled(locatorStoreTechnicalContactNumber, nameTechnicalContactNumber)) {
			return "Fail : Technical contact number is enabled in Store page";
		}

		// check if the avail hrs field is present in company page
		if (!FunctionLibrary.isElementDisplayed(locatorAvailHrsField, nameAvailHrsField)) {
			return "Fail : Avail hours field is not displayed in Store page";
		}*/

		// Check if the avail Hrs saturday field is prsent or not
		if (!FunctionLibrary.isElementDisplayed(locatorAvailHrsSatField, nameAvailHrsSatField)) {
			return "Fail : Avail hours saturday field is not displayed in Store page";
		}

		// Check if the avail Hrs sunday field is prsent or not
		if (!FunctionLibrary.isElementDisplayed(locatorAvailHrsSunField, nameAvailHrsSunField)) {
			return "Fail : Avail hours saturday field is not displayed in Store page";
		}

		// Check if the delete store button is displayed in company page
		if (!FunctionLibrary.isElementDisplayed(locatorDeleteStoreButton, nameDeleteStoreButton)) {
			return "Fail : Delete store button is not displayed in Store page";
		}

		// Check if the save store button is displayed in company page
		if (!FunctionLibrary.isElementDisplayed(locatorSaveStoreButton, nameSaveStoreButton)) {
			return "Fail : Save store button is not displayed in Store page";
		}

		return "Pass : Navigated to Store page present under Config management Menu";

	}

	// Create a browser instance and navigate to the Theatro logon tool
	public static String navigateToLogonTool() {

		System.out.println("Creating a browser instance and navigating to the logon tool ...");

		if (wbdv == null) {

			try {
				if (CONFIG.getProperty("test_browser").toLowerCase().contains("chrome")) {

					System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/chromedriver.exe");
					dc = DesiredCapabilities.chrome();
					wbdv = new ChromeDriver(dc);
					driver = new EventFiringWebDriver(wbdv);

					// Delete all browser cookies
					driver.manage().deleteAllCookies();

				}

				else if (CONFIG.getProperty("test_browser").toLowerCase().contains("firefox")
						|| CONFIG.getProperty("test_browser").toLowerCase().contains("ff")) {

					/*ProfilesIni allProfiles = new ProfilesIni();
					FirefoxProfile profile = allProfiles.getProfile("default");
					profile.setAcceptUntrustedCertificates(true);
					profile.setAssumeUntrustedCertificateIssuer(false);*/
					ProfilesIni allProfiles = new ProfilesIni();
					FirefoxProfile profile = allProfiles.getProfile("default");
					
					  profile.setPreference("network.proxy.http", "localhost");
					  profile.setPreference("network.proxy.http_port", 4446);
					  profile.setPreference("network.proxy.ssl", "localhost");
					  profile.setPreference("network.proxy.ssl_port", 4446);
					  profile.setPreference("network.proxy.type", 1);
					  
					  profile.setAcceptUntrustedCertificates(true);
					  profile.setAssumeUntrustedCertificateIssuer(true);
					  
					  DesiredCapabilities capabilities = new
								  DesiredCapabilities();
					  capabilities.setCapability(FirefoxDriver.PROFILE,
					  profile); profile.setAcceptUntrustedCertificates(true);
					  profile.setAssumeUntrustedCertificateIssuer(false);
					 

					profile.setPreference("network.proxy.type", ProxyType.AUTODETECT.ordinal());

					profile.setAcceptUntrustedCertificates(true);
					profile.setAssumeUntrustedCertificateIssuer(true);

					
					wbdv = new FirefoxDriver();
					driver = new EventFiringWebDriver(wbdv);

					// Delete all browser cookies
					driver.manage().deleteAllCookies();
				}

				else if (CONFIG.getProperty("test_browser").toLowerCase().contains("android")) {

					AppiumDriver driver = null;

					DesiredCapabilities capabilities = new DesiredCapabilities();

					capabilities.setCapability("deviceName", "Android");

					capabilities.setCapability("platformVersion", "4.2.2");

					driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

					driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

					// *******

					// Your testing code

					// ******

					driver.quit();

				}

			}

			catch (Throwable initBrowserException) {

				APPLICATION_LOGS
						.debug("Error came while creating a browser instance : " + initBrowserException.getMessage());

				return "Fail : Error came while creating a browser instance : " + initBrowserException.getMessage();

			}

		}

		APPLICATION_LOGS.debug("Created browser instance successfully");

		try {

			// Implicitly wait for 30 seconds for browser to open
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			// Navigate to Curaspan application
			driver.navigate().to(CONFIG.getProperty("logonToolURL"));

			// Maximize browser window
			APPLICATION_LOGS.debug("Maximizing Browser window...");
			driver.manage().window().maximize();
			APPLICATION_LOGS.debug("Browser window is maximized");
		}

		catch (Throwable navigationError) {

			APPLICATION_LOGS.debug("Error came while navigating to the logon tool : " + navigationError.getMessage());

		}
		// Verify elements in logon page is displayed or not
		if (!FunctionLibrary.isElementDisplayed(locatorLogonPageHeader, nameLogonPageHeader)) {
			return "Fail - Logon home page doesnot contain logon page header";
		}

		if (!FunctionLibrary.isElementDisplayed(locatorHomeButton, nameHomeButton)) {
			return "Fail - Logon home page doesnot contain home button";
		}

		if (!FunctionLibrary.isElementDisplayed(locatorLogonPageBackButton, nameLogonPageBackButton)) {
			return "Fail - Logon home page doesnot contain back button";
		}

		APPLICATION_LOGS.debug("Navigated to Logon home page");

		return "Pass : Navigated to Logon tool home page";

	}

	// enter all mandatory fields in add new company
	public static String enterMandatoryFieldsInAddCompanyPage(String companyName, String companyNickName,
			String primaryContactName, String primaryEmailID, String primaryContactNumber, String technicalContactName,
			String technicalEmailID, String technicalContactNumber) throws InterruptedException {

		APPLICATION_LOGS.debug("Add all mandatory fields in add new company page");

		// Clear and input company name
		methodReturnResult = FunctionLibrary.clearField(locatorNameField, nameNameField);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorNameField, nameNameField, companyName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input company nick name
		methodReturnResult = FunctionLibrary.clearField(locatorNickNameField, nameNickNameField);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorNickNameField, nameNickNameField, companyNickName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input primary contact name
		methodReturnResult = FunctionLibrary.clearField(locatorPrimaryContactName, namePrimaryContactName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorPrimaryContactName, namePrimaryContactName,
				primaryContactName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input primary mail ID
		methodReturnResult = FunctionLibrary.clearField(locatorPrimaryContactMailID, namePrimaryContactMailID);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorPrimaryContactMailID, namePrimaryContactMailID,
				primaryEmailID);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input primary contact number
		methodReturnResult = FunctionLibrary.clearField(locatorPrimaryContactNumber, namePrimaryContactNumber);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorPrimaryContactNumber, namePrimaryContactNumber,
				primaryContactNumber);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input technical contact name
		methodReturnResult = FunctionLibrary.clearField(locatorTechnicalContactName, nameTechnicalContactName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorTechnicalContactName, nameTechnicalContactName,
				technicalContactName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input technical mail ID
		methodReturnResult = FunctionLibrary.clearField(locatorTechnicalContactMailID, nameTechnicalContactMailID);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorTechnicalContactMailID, nameTechnicalContactMailID,
				technicalEmailID);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input technical contact number
		methodReturnResult = FunctionLibrary.clearField(locatorTechnicalContactNumber, nameTechnicalContactNumber);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorTechnicalContactNumber, nameTechnicalContactNumber,
				technicalContactNumber);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Click on save button
		methodReturnResult = FunctionLibrary.clickLink(locatorSaveButton, nameSaveButton);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		return "Pass : Entered all mandatory fields in Add new company page and clicked on save button";

	}

	// enter all mandatory fields in add new company
	public static String enterMandatoryFieldsInAddStorePage(String storeName, String storeNickName, String city, String country, String state,
			String postalCode, String address1, String storePhone, String primaryContactName, String primaryEmailID,
			String primaryContactNumber, String technicalContactName, String technicalEmailID,
			String technicalContactNumber) throws InterruptedException {

		APPLICATION_LOGS.debug("Add all mandatory fields in add new store page");

		// Clear and input company name
		methodReturnResult = FunctionLibrary.clearField(locatorNameField, nameNameField);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorNameField, nameNameField, storeName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input company nick name
		methodReturnResult = FunctionLibrary.clearField(locatorStoreNickNameField, nameNickNameField);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorStoreNickNameField, nameNickNameField, storeNickName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input city field
		methodReturnResult = FunctionLibrary.clearField(locatorCityField, nameCityField);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorCityField, nameCityField, city);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input postal field
		methodReturnResult = FunctionLibrary.clearField(locatorPostalCodeField, namePostalCodeField);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorPostalCodeField, namePostalCodeField, postalCode);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// clear and input country field
		methodReturnResult = FunctionLibrary.clearField(locatorCountryDropdown, nameCountryDropdown);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		methodReturnResult = FunctionLibrary.input(locatorCountryDropdown, nameCountryDropdown, country);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// clear and input state field
		methodReturnResult = FunctionLibrary.clearField(locatorStateDropdown, nameStateDropdown);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		methodReturnResult = FunctionLibrary.input(locatorStateDropdown, nameStateDropdown, state);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		

		// Clear and input store address1 field
		methodReturnResult = FunctionLibrary.clearField(locatorStoreAddress1Field, nameStoreAddress1Field);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorStoreAddress1Field, nameStoreAddress1Field, address1);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input store address1 field
		methodReturnResult = FunctionLibrary.clearField(locatorStoreAddress1Field, nameStoreAddress1Field);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorStoreAddress1Field, nameStoreAddress1Field, address1);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input store phone number field
		methodReturnResult = FunctionLibrary.clearField(locatorStorePhoneField, nameStorePhoneField);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorStorePhoneField, nameStorePhoneField, storePhone);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Verify the presence of store phone field
		if (!FunctionLibrary.isElementDisplayed(locatorStorePhoneField, nameStorePhoneField)) {
			return "Fail : Store phone input field is not displayed in store page";
		}

		// Clear and input primary contact name
		methodReturnResult = FunctionLibrary.clearField(locatorPrimaryContactName, namePrimaryContactName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorPrimaryContactName, namePrimaryContactName,
				primaryContactName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input primary mail ID
		methodReturnResult = FunctionLibrary.clearField(locatorStorePrimaryContactMailID, namePrimaryContactMailID);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorStorePrimaryContactMailID, namePrimaryContactMailID,
				primaryEmailID);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input primary contact number
		methodReturnResult = FunctionLibrary.clearField(locatorStorePrimaryContactNumber, namePrimaryContactNumber);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorStorePrimaryContactNumber, namePrimaryContactNumber,
				primaryContactNumber);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input technical contact name
		methodReturnResult = FunctionLibrary.clearField(locatorStoreTechnicalContactName, nameTechnicalContactName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorStoreTechnicalContactName, nameTechnicalContactName,
				technicalContactName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input technical mail ID
		methodReturnResult = FunctionLibrary.clearField(locatorStoreTechnicalContactMailID, nameTechnicalContactMailID);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorStoreTechnicalContactMailID, nameTechnicalContactMailID,
				technicalEmailID);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input technical contact number
		methodReturnResult = FunctionLibrary.clearField(locatorStoreTechnicalContactNumber, nameTechnicalContactNumber);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorStoreTechnicalContactNumber, nameTechnicalContactNumber,
				technicalContactNumber);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Click on save button
		methodReturnResult = FunctionLibrary.clickLink(locatorSaveStoreButton, nameSaveStoreButton);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorStoreSaveConfirmation);
		
		// click on Ok button
		methodReturnResult = FunctionLibrary.clickLink(locatorOkButton, nameOkButton);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorNameField);
		

		return "Pass : Entered all mandatory fields in Add new store page and clicked on save button";

	}

	// Navigate to store page present under config management menu
	public static String navigateToEmployeePage() throws InterruptedException {

		APPLICATION_LOGS.debug("Navigate To employee page and verify all the elements");

		// Click on config management menu
	/*	methodReturnResult = FunctionLibrary.clickLink(locatorConfigManagementMenuOption,
				nameConfigManagementMenuOption);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Click on Employee link
		methodReturnResult = FunctionLibrary.clickAndWait(locatorEmployeeOptionUnderConfigManagementMenu,
				nameEmployeeOptionUnderConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

//		// Wait for page to load
//		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);

		// Verify whether page is navigated to employee page
		// Verify panel header
		methodReturnResult = FunctionLibrary.assertText("Employee Header",
				driver.findElement(locatorEmployeeHeaderPanel).getText(), Constants.employeePageHeader);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		} */
		
		
		Thread.sleep(2000);
		FunctionLibrary.waitForElementToLoad(locatorConfigManagementMenuOption);

		// Verify merchant dropdown is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorMerchantDropdownInStorePage, nameMerchantDropdownInStorePage)) {
			return "Fail : Mercahnt dropdown in employee page is not displayed";
		}

		// Verify store dropdown is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorStoreDropdownInEmployeePage, nameStoreDropdownInStorePage)) {
			return "Fail : Store dropdown in employee page is not displayed";
		}

		// Verify user name search field is present in the page
		/*if (!FunctionLibrary.isElementDisplayed(locatorUserNameFieldInEmployeePage, nameUserNameFieldInEmployeePage)) {
			return "Fail : User name field in employee page is not displayed";
		}*/

		// Verify add new employee link is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorNewEmployeeLink, nameNewEmployeeLink)) {
			return "Fail : Add new employee link in employee page is not displayed";
		}

		return "Pass : Navigated to Store page present under Config management Menu";

	}

	// Navigate to add new employee page and verify all the elements in the
	// popup
	public static String navigateToAddNewEmployeePage() throws InterruptedException {

		APPLICATION_LOGS.debug("Navigate To add new employee page and verify all the elements in the popup");

		Thread.sleep(5000);

		// Wait for the page to load
		FunctionLibrary.waitForPageToLoad();

		// Click on add new employee link
		methodReturnResult = FunctionLibrary.clickLink(locatorAddNewEmployeeLink, nameAddNewEmployeeLink);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Wait for add employee popup to appear
		FunctionLibrary.waitForElementToLoad(locatorAddEmployeePopup);

		// Verify the presence of add new employee popup
		if (!FunctionLibrary.isElementDisplayed(locatorAddEmployeePopup, nameAddEmployeePopup)) {
			return "Fail : Add employee popup in employee page is not displayed";
		}

		// Verify first name field is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorEmployeeFirstNameInAddEmployeePopup,
				nameEmployeeFirstNameInAddEmployeePopup)) {
			return "Fail : First name field in add employee popup is not displayed";
		}

		// Verify last name field is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorEmployeeLastNameInAddEmployeePopup,
				nameEmployeeLastNameInAddEmployeePopup)) {
			return "Fail : Last name field in employee page is not displayed";
		}

		// Verify prefered name field is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorEmployeePreferredNameInAddEmployeePopup,
				nameEmployeePreferredNameInAddEmployeePopup)) {
			return "Fail : Last name field in employee page is not displayed";
		}

		// Verify custom employee ID field is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorCustomEmployeeIDInAddEmployeePopup,
				nameCustomEmployeeIDInAddEmployeePopup)) {
			return "Fail : custom employee ID in employee page is not displayed";
		}

		// Verify profile ID field is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorProfileIDInAddEmployeePopup, nameProfileIDInAddEmployeePopup)) {
			return "Fail : profile ID in employee page is not displayed";
		}

		// Verify the presence of groups tab in add employee popup page
		if (!FunctionLibrary.isElementDisplayed(locatorGroupsTab, nameGroupsTab)) {
			return "Fail : Groups tab in add employee popup is not displayed";
		}

		// verify the presence of Cancel button
		if (!FunctionLibrary.isElementDisplayed(locatorCancelButton, nameCancelButton)) {
			return "Fail : Cancel button in add employee popup is not displayed";
		}

		// verify the presence of Save button
		if (!FunctionLibrary.isElementDisplayed(locatorSaveEmployeeButton, nameSaveEmployeeButton)) {
			return "Fail : Save button in add employee popup is not displayed";
		}
		
		return "Pass : Navigated to add employee page and verify all the elements present in the popup";

	}

	// enter all mandatory fields in add new employee
	public static String enterMandatoryFieldsInAddEmployeePopup(String employeeFirstName, String employeeLastName)
			throws InterruptedException {

		APPLICATION_LOGS.debug("Add all mandatory fields in add new employee page");

		// Clear and input employee first name
		methodReturnResult = FunctionLibrary.clearField(locatorEmployeeFirstNameInAddEmployeePopup,
				nameEmployeeFirstNameInAddEmployeePopup);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorEmployeeFirstNameInAddEmployeePopup,
				nameEmployeeFirstNameInAddEmployeePopup, employeeFirstName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Clear and input employee last name
		methodReturnResult = FunctionLibrary.clearField(locatorEmployeeLastNameInAddEmployeePopup,
				nameEmployeeLastNameInAddEmployeePopup);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		methodReturnResult = FunctionLibrary.input(locatorEmployeeLastNameInAddEmployeePopup,
				nameEmployeeLastNameInAddEmployeePopup, employeeLastName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Click on save button
		methodReturnResult = FunctionLibrary.clickLink(locatorSaveEmployeeButton, nameSaveEmployeeButton);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}	
					

		return "Pass : Entered all mandatory fields in Add new employee popup and clicked on save button";

	}

	// Navigate to store page present under config management menu
	public static String navigateToEmployeeGrammarPage() throws InterruptedException {

		APPLICATION_LOGS.debug("Navigate To employee page and verify all the elements");

		// Click on config management menu
		methodReturnResult = FunctionLibrary.clickLink(locatorConfigManagementMenu,
				nameConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Click on Grammar link
		methodReturnResult = FunctionLibrary.clickLink(locatorGrammarLinkUnderConfigManagementMenu,
				nameGrammarLinkUnderConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Click on employee Grammar link
		methodReturnResult = FunctionLibrary.clickLink(locatorEmployeeGrammarLink, nameEmployeeGrammarLink);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		//Wait for load text to appear
		FunctionLibrary.waitForElementToLoad(locatorLoadingText);
		
		// Wait for page to load
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);

		// Verify whether page is navigated to employee page
		// Verify panel header
		methodReturnResult = FunctionLibrary.assertText("Employee Grammar Header",
				driver.findElement(locatorEmployeeGrammarHeaderPanel).getText(), Constants.employeeGrammarPageHeader);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Verify merchant dropdown is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorMerchantDropdownInStorePage, nameMerchantDropdownInStorePage)) {
			return "Fail : Mercahnt dropdown in employee page is not displayed";
		}

		// Verify store dropdown is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorStoreDropdownInEmployeePage, nameStoreDropdownInStorePage)) {
			return "Fail : Store dropdown in employee page is not displayed";
		}

		// Verify employee id column link
		if (!FunctionLibrary.isElementDisplayed(locatorEmployeeIDColumnLinkInEmployeeGrammarPage,
				nameEmployeeIDColumnLinkInEmployeeGrammarPage)) {
			return "Fail : employee id column link in employee grammar page is not displayed";
		}

		// Verify first name column link
		if (!FunctionLibrary.isElementDisplayed(locatorFirstNameColumnLinkInEmployeeGrammarPage,
				nameFirstNameColumnLinkInEmployeeGrammarPage)) {
			return "Fail : first name column link in employee grammar page is not displayed";
		}

		// Verify preferred name column link
		if (!FunctionLibrary.isElementDisplayed(locatorPreferredNameColumnLinkInEmployeeGrammarPage,
				namePreferredNameColumnLinkInEmployeeGrammarPage)) {
			return "Fail : preferred name column link in employee grammar page is not displayed";
		}

		// Verify last name column link
		if (!FunctionLibrary.isElementDisplayed(locatorLastNameColumnLinkInEmployeeGrammarPage,
				nameLastNameColumnLinkInEmployeeGrammarPage)) {
			return "Fail : last name column link in employee grammar page is not displayed";
		}

		// Verify first name alias column link
		if (!FunctionLibrary.isElementDisplayed(locatorFirstNameAliasColumnLinkInEmployeeGrammarPage,
				nameFirstNameAliasColumnLinkInEmployeeGrammarPage)) {
			return "Fail : first name alias column link in employee grammar page is not displayed";
		}

		// Verify last name alias column link
		if (!FunctionLibrary.isElementDisplayed(locatorLastNameAliasColumnLinkInEmployeeGrammarPage,
				nameLastNameAliasColumnLinkInEmployeeGrammarPage)) {
			return "Fail : last name alias column link in employee grammar page is not displayed";
		}

		return "Pass : Navigated to employee grammar page present under Config management Menu";

	}

	// Navigate to groups page present under config management menu
	public static String navigateToGroupsPage() throws InterruptedException {

		APPLICATION_LOGS.debug("Navigate To groups page and verify all the elements");
		
		// wait for page load
				FunctionLibrary.waitForPageToLoad();
						 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
				
		Thread.sleep(2000);
		//wait for element for to element
		FunctionLibrary.waitForElementToLoad(locatorConfigManagementMenu);
		
		FunctionLibrary.waitForElementToLoad(locatorGroupManagementMenu);

		// Click on group management menu
		methodReturnResult = FunctionLibrary.clickAndWait(locatorGroupManagementMenu,
				nameGroupManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Click on Group link
		methodReturnResult = FunctionLibrary.clickAndWait(locatorGroupsOptionUnderConfigManagementMenu,
				nameGroupsOptionUnderConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// Wait for group header to load
				FunctionLibrary.waitForElementToLoad(locatorAddNewGroupLinkInGroupsPage);

		// Wait for page to load
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		// Verify whether page is navigated to group page
		// Verify panel header
		methodReturnResult = FunctionLibrary.assertText("Group Header",
				driver.findElement(locatorGroupHeaderPanel).getText(), Constants.groupPageHeader);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Verify merchant dropdown is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorMerchantDropdownInStorePage, nameMerchantDropdownInStorePage)) {
			return "Fail : Mercahnt dropdown in Groups page is not displayed";
		}

		// Verify store dropdown is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorStoreDropdownInEmployeePage, nameStoreDropdownInStorePage)) {
			return "Fail : Store dropdown in Groups page is not displayed";
		}

	/*	// Verify group dropdown is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorGroupDropdownInGroupsPage, nameGroupDropdownInGroupsPage)) {
			return "Fail : User name field in Groups page is not displayed";
		}  */

		// Verify add new group link is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorAddNewGroupLinkInGroupsPage, nameAddNewGroupLinkInGroupsPage)) {
			return "Fail : Add new group link in Groups page is not displayed";
		}

		// Verify group name field is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorGroupNameFieldInGroupsPage, nameGroupNameFieldInGroupsPage)) {
			return "Fail : Group name field in Groups page is not displayed";
		}

		// Verify group users tab is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorGroupUsersTabInGroupsPage, nameGroupUsersTabInGroupsPage)) {
			return "Fail : Group users tab in Groups page is not displayed";
		}

		// Verify voice tab is present in the page
		/*if (!FunctionLibrary.isElementDisplayed(locatorVoiceTabInGroupsPage, nameVoiceTabInGroupsPage)) {
			return "Fail : Voice tab in Groups page is not displayed";
		}*/
		
		
        WebElement element = driver.findElement(locatorGroupDeleteBtnInGroupsPage);
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", element);
		
		
		
		Thread.sleep(5000);
		// Verify add new user link is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorAddNewUserLinkInGroupsPage, nameAddNewUserLinkInGroupsPage)) {
			return "Fail : Add new user link in Groups page is not displayed";
		}

		// Verify delete user link is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorDeleteUserLinkInGroupsPage, nameDeleteUserLinkInGroupsPage)) {
			return "Fail : Delete user link in Groups page is not displayed";
		}

		/*Thread.sleep(3000);
		// Verify group delete button is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorGroupDeleteBtnInGroupsPage, nameGroupDeleteBtnInGroupsPage)) {
			return "Fail : Delete group button in Groups page is not displayed";
		}*/

		// Verify group save button is present in the page
		if (!FunctionLibrary.isElementDisplayed(locatorGroupSaveBtnInGroupsPage, nameGroupSaveBtnInGroupsPage)) {
			return "Fail : Save group button in Groups page is not displayed";
		}

		return "Pass : Navigated to Group page present under Config management Menu";

	}
	
	// Navigate to Sales Update page under config manangement

	public static String navigateToSalesPage() throws BiffException, IOException, InterruptedException {


		APPLICATION_LOGS.debug("Navigate To Sales Update page under Config management menu");
		
		
		// Wait for page to load
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);

		// Validate the inputs to the sales page
		// Retrieve data from excel for basic profile
	
		String companyName = testData.getCellData("Sales", "CompanyName", 1);
		
		String storeName = testData.getCellData("Sales", "StoreName", 1);
		
		Thread.sleep(3000);
		// Click on Config Management Menu
		methodReturnResult = FunctionLibrary.clickLink(locatorConfigManagementMenu, nameConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		
		Thread.sleep(2000);
		// Click on Sales Update option
		methodReturnResult = FunctionLibrary.clickLink(locatorSalesOption, nameSalesOption);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// Wait for page to load
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		
		
		/* Check for the Sales  header in Sales page

		if (!FunctionLibrary.isElementDisplayed(locatorSalesUpdateHeader, nameSalesUpdate)) {
			return "Fail : Sales Update Option under Config Management   is not displayed ";
		}*/
		
		// Check for the Admin Button in Sales page

		if (!FunctionLibrary.isElementDisplayed(locatorAdminButtonInSales, nameAdminButtonInSales)) {
			return "Fail : Admin Button is not displayed in Sales Update Page ";
		}

		
		// Check for the company drop down in Sales page

		if (!FunctionLibrary.isElementDisplayed(locatorCompanyDropDn, nameCompanyDropDn)) {
			return "Fail : Company drop down is not displayed in Sales update ";
		}

		// Check for store drop down in Sales page
		if (!FunctionLibrary.isElementDisplayed(locatorStoreDropDn, nameStoreDropDn)) {
			return "Fail : Store drop down is not displayed in Sales update";
		}
		
		// Check for Add reminder time in Sales page
		if (!FunctionLibrary.isElementDisplayed(locatorReminderTimeButton, nameReminderTimeButton)) {
				return "Fail : Add reminder time is not displayed in Sales update";
		}
		
		
		Thread.sleep(3000);
		//wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorReminderTimeButton);
		

		Thread.sleep(3000);
		// Check for Reminder timer  Sales page by manual entry
		/*int j=1; // variable used to increment the time value
		for (int i=2 ; i <= 4; i++) {

			APPLICATION_LOGS.debug(
					"In For Loop Sales Update reminder i: "+i);

			if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='sales-update-timestamps-view']/div["+i+"]/div/label"), "Sales Update reminder")) {
				return "Fail : Sales update reminder drop down is not displayed in Sales update";
			}
			
			// Validate the input to reminder 
			
			String Inputreminder = TestUtil.getlocalhour()+":"+(TestUtil.getlocalmin()+j);
			
			methodReturnResult = FunctionLibrary.clearField(By.xpath(".//*[@id='sales-update-timestamps-view']/div["+i+"]/div/div/input"), "Sales update reminer clear");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			j++;
			
			APPLICATION_LOGS.debug(
					"In For Loop Sales Update reminder Inputreminder:"+Inputreminder);
			APPLICATION_LOGS.debug(
					"In For Loop Sales Update reminder j:"+j);
			
			
			methodReturnResult = FunctionLibrary.input(By.xpath(".//*[@id='sales-update-timestamps-view']/div["+i+"]/div/div/input"), Inputreminder, "Sales update reminer input");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
	
			
			
		}*/
		
		
		

		
		// Check for Save button
		
		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='saveSalesUpdate']"), "Save Button")) {
			return "Fail : Save button  is not displayed in Sales update";
		}
		
		// Submit on Save Button
		/*		methodReturnResult = FunctionLibrary.clickLink(By.xpath(".//*[@id='saveSalesUpdate']"), "Save Button");
				if (methodReturnResult.contains("Fail"))
					return methodReturnResult;  */
				
		// Select Company name

		methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCompanyName, companyName, nameCompanyName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Select Store name

		methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorStoreName, storeName, nameStoreName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// Wait for page to load
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		Thread.sleep(3000);
		//wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorReminderTimeButton);
		
		//click on Add Reminder time
		methodReturnResult = FunctionLibrary.clickLink(locatorReminderTimeButton, nameReminderTimeButton);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		

		// Validate the inner box of the reminder time another way of providing input for the reminder time
		 // variable used to increment the time value
		
			APPLICATION_LOGS.debug(
					"In For Loop Sales Update reminder providing input via inner box ");
						
			// Validate the input to reminder 

			int Inputhr = TestUtil.getlocalhour();
			int Inputmin = TestUtil.getlocalmin();
			
			System.out.println("CurrentTime: "+Inputhr+":"+Inputmin);
		
			APPLICATION_LOGS.debug(
					"In For Loop Sales Update reminder   providing input via inner box:");
			// Clear the box
			methodReturnResult = FunctionLibrary.clickLink(By.xpath(".//*[@id='sales-update-timestamps-view']/div/div/div/input"), "Sales update reminder time input box");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			Thread.sleep(3000);
			methodReturnResult = FunctionLibrary.clickLink(By.xpath("html/body/div/table/tbody/tr[2]/td[1]/input"), "Sales update reminer input hour");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			
			
			methodReturnResult = FunctionLibrary.clearField(By.xpath("html/body/div/table/tbody/tr[2]/td[1]/input"), "Sales update reminer input hour");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			methodReturnResult = FunctionLibrary.clickLink(By.xpath("html/body/div/table/tbody/tr[2]/td[3]/input"), "Sales update reminer input Min");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			
			
			methodReturnResult = FunctionLibrary.clearField(By.xpath("html/body/div/table/tbody/tr[2]/td[3]/input"), "Sales update reminer input Min");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			
			
			methodReturnResult = FunctionLibrary.input(By.xpath("html/body/div/table/tbody/tr[2]/td[1]/input"), "Sales update reminer input hour", ""+Inputhr+"");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			methodReturnResult = FunctionLibrary.input(By.xpath("html/body/div/table/tbody/tr[2]/td[3]/input"), "Sales update reminer input min", ""+Inputmin+"");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			methodReturnResult = FunctionLibrary.clickLink(By.xpath(".//*[@id='sales-update-timestamps-view']/div/div/div/input"), "Sales update reminder time input box");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			String Inputbox = Inputhr + ":" + Inputmin ;
			APPLICATION_LOGS.debug("Sales Update comparison Inputbox :"+Inputbox);
			String Button = driver.findElement(By.xpath(".//*[@id='sales-update-timestamps-view']/div/div/div/input")).getAttribute("value");
			APPLICATION_LOGS.debug("Sales Update in the button :" +Button);
			Thread.sleep(3000);
			if (Inputbox.equalsIgnoreCase(Button)){
				
				APPLICATION_LOGS.debug("Sales Update Values for Inputbox and Button: "+Inputbox +" and "+ Button);
				
			}
			
			Thread.sleep(3000);
			methodReturnResult = FunctionLibrary.clickLink(locatorAdminButtonInSales, nameAdminButtonInSales);
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			/*Thread.sleep(3000);
			WebDriver driver = new FirefoxDriver();
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,250)", "");*/
			
		
			Thread.sleep(3000);
		// Submit on Save Button
		/*methodReturnResult = FunctionLibrary.clickLink(By.xpath(".//*[@id='saveSalesUpdate']"), "Save Button");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		//wait for element to load
		FunctionLibrary.waitForElementToLoad(By.xpath("//*[@class='bootbox-body'][contains(text(),'Saved Sales Update Successfully')]"));
		
		//click on OK button
		methodReturnResult = FunctionLibrary.clickLink(locatorSalesUpdateSuccessSaveMessage, nameSalesUpdateSuccessSaveMessage);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}*/
		
		return "Pass : Navigated to Sales Update page under Config management Menu";

	}
	
	public static String navigateToHuddlePage() throws BiffException, IOException, InterruptedException {
		
		APPLICATION_LOGS.debug("Navigate To Huddle  page under Config management menu");

		// Validate the inputs to the Huddle page
		// Retrieve data from excel for Huddle sheet
	
		String companyName = testData.getCellData("Huddle", "CompanyName", 1);

		String storeName = testData.getCellData("Huddle", "StoreName", 1);

		String  huddlegroupName = testData.getCellData("Huddle", "HuddleGroup", 1);
		String huddeleName = testData.getCellData("Huddle", "HuddleGroup", 2);

		Thread.sleep(3000);
		// Click on Config Management Menu
		methodReturnResult = FunctionLibrary.clickLink(locatorConfigManagementMenu, nameConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		Thread.sleep(2000);
		// Click on huddle option
		methodReturnResult = FunctionLibrary.clickLink(locatorHuddle, nameHuddle);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		//Validate all the buttons are present in the huddle page
		// Check for the Admin Button in Huddle page

		if (!FunctionLibrary.isElementDisplayed(locatorAdminButtonInSales, nameAdminButtonInSales)) {
			return "Fail : Admin Button is not displayed in Huddle  Page ";
		}
		
		// Check for the company drop down in Huddle page

		if (!FunctionLibrary.isElementDisplayed(locatorCompanyDropDn, nameCompanyDropDn)) {
			return "Fail : Company drop down is not displayed in Huddle Page ";
		}

		// Check for store drop down in Huddle page
		if (!FunctionLibrary.isElementDisplayed(locatorStoreDropDn, nameStoreDropDn)) {
			return "Fail : Store drop down is not displayed in Huddle Page";
		}
		
		// Check for Added new button
		
		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='addNewHuddle']"), "Added new Huddle Button")) {
			return "Fail : Delete Huddle button  is not displayed in Huddle Page";
		}
		
		
		// Check for Name of huddle 
		
		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='groupForm']/div/div[1]/label"), "Name of Huddle ")) {
					return "Fail : Name of  Huddle   is not displayed in Huddle Page";
				}
	
		
		// Check for Place holder Name of huddle 
		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='name']"), "Place holder for Name of Huddle ")) {
			return "Fail : Place holder for Name of  Huddle   is not displayed in Huddle Page";
		}
		
		// Check for Start time button 
		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='groupForm']/div/div[3]/label"), "Start time of Huddle ")) {
			return "Fail : Start time of  Huddle   is not displayed in Huddle Page";
		}
		
		// Check for Start time place holder 
		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='huddleStartTime']"), "Start time Place holder of Huddle ")) {
			return "Fail : Start time Place holder   is not displayed in Huddle Page";
		}

		
		// Check for Stop time button 
		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='groupForm']/div/div[4]/label"), "Stop time of Huddle ")) {
			return "Fail : Stop time of  Huddle   is not displayed in Huddle Page";
		}
		
		// Check for Stop time place holder 
		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='huddleStopTime']"), "Stop time Place holder of Huddle ")) {
			return "Fail : Stop time Place holder   is not displayed in Huddle Page";
		}
		
		
		// Check for Reminder time button 
		if (!FunctionLibrary.isElementDisplayed(By.id("addHuddleReminder"), "Reminder time of Huddle ")) {
			return "Fail : Reminder time of  Huddle   is not displayed in Huddle Page";
		}
		
		
		// Check for Add Group Button  
		if (!FunctionLibrary.isElementDisplayed(By.id("addIssuerGroups"), "Add Group Button")) {
			return "Fail : Add Group Button is not displayed in Huddle Page";
		}
		
		// Check for Remove Group 
		if (!FunctionLibrary.isElementDisplayed(By.id("removeIssuerGrp"), "Remove Group")) {
			return "Fail : Remove Group is not displayed in Huddle Page";
		}
		
		// Check for Add member   
		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='addIssuerEmployees']"), "Add member")) {
			return "Fail : Add member in User list is not displayed in Huddle Page";
		}
		
		// Check for Remove member 
		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='removeIssuerEmp']"), "Remove member")) {
			return "Fail : Remove member in  User list is not displayed in Huddle Page";
		}
		// Check for Delete button

		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='huddleDelete']"), "Delete Huddle Button")) {
			return "Fail : Delete Huddle button  is not displayed in Huddle Page";
		}

		// Check for Save button
		
		if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='saveHuddleButton']"), "Save Button in Huddle")) {
			return "Fail : Save button  is not displayed in Huddle Page";
		}
		
		// Validate the inputs to Huddle page

		
		// Select Company name

		methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCompanyName, companyName, nameCompanyName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		// Select Store name

		methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorStoreName, storeName, nameStoreName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		
		String text = driver.findElement(locatorGroupListContainer).getText();
		String textElement = "No existing huddles found.";
		
		if(text.equals(textElement)) {
			
			// Validate the input to reminder 

			int Inputhr = TestUtil.getlocalhour();
			int Inputmin = TestUtil.getlocalmin();
			int Inputsec = TestUtil.getlocalsec();
			
			String Inputbox = Inputhr + ":" + Inputmin;
			
			
			methodReturnResult = FunctionLibrary.input(locatorHuddleName, nameHuddleName, huddeleName);
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
	         
			Thread.sleep(3000);
			// Clear the box and provide input to start time 
			methodReturnResult = FunctionLibrary.clickLink(By.xpath(".//*[@id='huddleStartTime']"), "Huddle  start time ");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}

			Thread.sleep(3000);
			methodReturnResult = FunctionLibrary.clearField(By.xpath(".//*[@id='huddleStartTime']"), "Huddle  start time");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}


			Thread.sleep(3000);
			methodReturnResult = FunctionLibrary.input(By.xpath(".//*[@id='huddleStartTime']"), "Huddle  start time", ""+Inputbox+"");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			
			// Submit on Save Button
			methodReturnResult = FunctionLibrary.clickLink(By.xpath(".//*[@id='saveHuddleButton']"), "Save Button");
			if (methodReturnResult.contains("Fail")) {
				return methodReturnResult;
			}
			
			String tagout = driver.findElement(By.xpath("//*[@class='bootbox-body'][contains(text(),'No Tagouts configured in the DataBase.Please configure.')]")).getText();
			String tagoutexpect = "No Tagouts configured in the DataBase.Please configure.";
			
			if(tagout.equals(tagoutexpect)) {
			//click on OK button
			methodReturnResult = FunctionLibrary.clickLink(locatorSalesUpdateSuccessSaveMessage, nameSalesUpdateSuccessSaveMessage);
			if (methodReturnResult.contains("Fail")) {
		    		return methodReturnResult;
			}
		  }
			
			
		}
		
		else {

		// Select Huddle group name

		methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorHuddlegroupName, huddlegroupName, nameHuddlegroupName);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// Validate the input to reminder 

		int Inputhr = TestUtil.getlocalhour();
		int Inputmin = TestUtil.getlocalmin();
		int Inputsec = TestUtil.getlocalsec();
		
		String Inputbox = Inputhr + ":" + Inputmin;
         
		Thread.sleep(3000);
		// Clear the box and provide input to start time 
		methodReturnResult = FunctionLibrary.clickLink(By.xpath(".//*[@id='huddleStartTime']"), "Huddle  start time ");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		Thread.sleep(3000);
		methodReturnResult = FunctionLibrary.clearField(By.xpath(".//*[@id='huddleStartTime']"), "Huddle  start time");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}


		Thread.sleep(3000);
		methodReturnResult = FunctionLibrary.input(By.xpath(".//*[@id='huddleStartTime']"), "Huddle  start time", ""+Inputbox+"");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		Inputhr = Inputhr + 5;
		Inputmin = Inputmin + 5;
		Inputsec = Inputsec + 5;

		Inputbox = Inputhr + ":" + Inputmin;
		Thread.sleep(3000);
		// Clear the box and provide input to stop time
		methodReturnResult = FunctionLibrary.clickLink(By.xpath(".//*[@id='huddleStopTime']"), "Huddle  stop time ");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		Thread.sleep(3000);
		methodReturnResult = FunctionLibrary.clearField(By.xpath(".//*[@id='huddleStopTime']"), "Huddle  stop time");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}


		Thread.sleep(3000);
		methodReturnResult = FunctionLibrary.input(By.xpath(".//*[@id='huddleStopTime']"), "Huddle  stop time", ""+Inputbox+"");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		Thread.sleep(3000);
		methodReturnResult = FunctionLibrary.clickLink(locatorAdminButtonInSales, nameAdminButtonInSales);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		
		Inputhr = Inputhr - 6;
		Inputmin = Inputmin;
		Inputsec = Inputsec + 2;

		Inputbox = Inputhr + ":" + Inputmin;
		Thread.sleep(3000);
		// Clear the box and provide input to reminder time
		methodReturnResult = FunctionLibrary.clickLink(By.xpath(".//*[@id='reminder1']"), "Huddle  reminder time ");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

		Thread.sleep(3000);
		methodReturnResult = FunctionLibrary.clearField(By.xpath("/html/body/div/table/tbody/tr[2]/td[1]/input"), "clear Huddle  reminder time hour");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

        Thread.sleep(3000);
		methodReturnResult = FunctionLibrary.input(By.xpath("/html/body/div/table/tbody/tr[2]/td[1]/input"), "Huddle  reminder time", ""+Inputhr+"");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		
		Thread.sleep(3000);
		methodReturnResult = FunctionLibrary.clearField(By.xpath("/html/body/div/table/tbody/tr[2]/td[3]/input"), "clear Huddle  reminder time min");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}

        Thread.sleep(3000);
		methodReturnResult = FunctionLibrary.input(By.xpath("/html/body/div/table/tbody/tr[2]/td[3]/input"), "Huddle  reminder time", ""+Inputmin+"");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		
		Thread.sleep(15000);
		// Click on issuers all employee checkbox
		/*methodReturnResult = FunctionLibrary.clickLink(By.xpath(".//*[@id='issuersAll']"), "Issuers all check box is present in Huddle page ");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}*/
		
	  
		
		// Submit on Save Button
		methodReturnResult = FunctionLibrary.clickLink(By.xpath(".//*[@id='saveHuddleButton']"), "Save Button");
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		
		//wait for element to load
				FunctionLibrary.waitForElementToLoad(By.xpath("//*[@class='bootbox-body'][contains(text(),'Huddle saved successfully.')]"));
				
				//wait for element to load
				FunctionLibrary.waitForElementToLoad(locatorHuddleSuccessSaveMessage);
				
				//click on OK button
				methodReturnResult = FunctionLibrary.clickLink(locatorSalesUpdateSuccessSaveMessage, nameSalesUpdateSuccessSaveMessage);
				if (methodReturnResult.contains("Fail")) {
			    		return methodReturnResult;
				}
				
		}
		
		Thread.sleep(10000);
		return "Pass : Navigated to Huddle  page under Config management Menu";
  }
	
	public static String navigateToAlarmDefinitionPage() throws BiffException, IOException {
	
		// Validate the inputs to the Alarm Definition  page
				// Retrieve data from excel for Huddle sheet
			
				String companyName = testData.getCellData("AlarmDefinition", "CompanyName", 1);

				String storeName = testData.getCellData("AlarmDefinition", "StoreName", 1);
				
		// Click on Fault Management Menu
				methodReturnResult = FunctionLibrary.clickLink(locatorFaultManagementMenu, nameFaultManagementMenu);
				if (methodReturnResult.contains("Fail")) {
					return methodReturnResult;
				}

				
				// Click on Alarm definition page
				methodReturnResult = FunctionLibrary.clickLink(locatorAlarmDefinitionOption, nameAlarmDefinition);
				if (methodReturnResult.contains("Fail")) {
					return methodReturnResult;
				}
				
				// Select Company name

				methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCompanyName, companyName, nameCompanyName);
				if (methodReturnResult.contains("Fail")) {
					return methodReturnResult;
				}

				// Select Store name

				methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorStoreName, storeName, nameStoreName);
				if (methodReturnResult.contains("Fail")) {
					return methodReturnResult;
				}

				
				//Validate the header 
				// Check for the Admin Button in Sales page

				if (!FunctionLibrary.isElementDisplayed(locatorAdminButtonInAlarmDefinition, nameAdminButtonInAlarmDefinition)) {
					return "Fail : Admin Button is not displayed in Alarm Definition  Page ";
				}
			// Check for the Alarm Definition header in alarm definition page

				//if (!FunctionLibrary.isElementDisplayed(locatorAlarmDefinitionHeader, nameAlarmDefinitionHeader)) {
					//return "Fail : Alarm Definition header is not displayed in Alarm Definition";
				//}
	
				// Check for ID button in Alarm definition page
				if (!FunctionLibrary.isElementDisplayed(locatorID, "ID in Alarm Definition Page")) {
					return "Fail : ID Button is not displayed in Alarm definition page";
				}
				
				// Check for Description button in Alarm definition page
				if (!FunctionLibrary.isElementDisplayed(By.xpath("//th[contains(text(),'Description')]"), "Description in Alarm Definition Page")) {
					return "Fail : Description Button is not displayed in Alarm definition page";
				}
				
				// Check for Category button in Alarm definition page
				if (!FunctionLibrary.isElementDisplayed(By.xpath("//a[contains(text(),'Category')]"), "Category in Alarm Definition Page")) {
					return "Fail : Category Button is not displayed in Alarm definition page";
				}
				
				// Check for Recovery button in Alarm definition page
				if (!FunctionLibrary.isElementDisplayed(By.xpath("//th[contains(text(),'Recovery')]"), "Recovery in Alarm Definition Page")) {
					return "Fail : Recovery Button is not displayed in Alarm definition page";
				}
				
				// Check for Severity button in Alarm definition page
				if (!FunctionLibrary.isElementDisplayed(By.xpath("//a[contains(text(),'Severity')]"), "Severity in Alarm Definition Page")) {
					return "Fail : Severity Button is not displayed in Alarm definition page";
				}
				
				// Check for Type button in Alarm definition page
				if (!FunctionLibrary.isElementDisplayed(By.xpath("//a[contains(text(),'Type')]"), "Type in Alarm Definition Page")) {
					return "Fail : Type Button is not displayed in Alarm definition page";
				}
				
				// Check for Edit button in Alarm definition page
				if (!FunctionLibrary.isElementDisplayed(By.xpath("//th[contains(text(),'Edit')]"), "Edit in Alarm Definition Page")) {
					return "Fail : Edit Button is not displayed in Alarm definition page";
				}
				
		return "Pass : Navigated to Alarm  Definition page under Fault management Menu";
	}
	
	public static String navigateToAlarmSubscriptionPage() throws BiffException, IOException {
		

				
		        // Click on Fault Management Menu
				methodReturnResult = FunctionLibrary.clickLink(locatorFaultManagementMenu, nameFaultManagementMenu);
				if (methodReturnResult.contains("Fail")) {
					return methodReturnResult;
				}

				
				// Click on Alarm definition page
				methodReturnResult = FunctionLibrary.clickLink(locatorAlarmSubscriptionOption, nameAlarmSubscription);
				if (methodReturnResult.contains("Fail")) {
					return methodReturnResult;
				}
				
				
				// Check for the Admin Button in Sales page

				if (!FunctionLibrary.isElementDisplayed(By.id("user-dropdown"), nameAdminButtonInAlarmDefinition)) {
					return "Fail : Admin Button is not displayed in Alarm Definition  Page ";
				}
				
				
				// Validate the add subscription button ?
				
				// Click on  link
				methodReturnResult = FunctionLibrary.clickLink(By.id("add-subscription"), "Add Subscription");
				if (methodReturnResult.contains("Fail")) {
					return methodReturnResult;
				}
				
				// Wait for page to load
//				FunctionLibrary.waitForElementToLoad(By.className("btn btn-success1"));
				
				//Validate the button in add subscription page
				
			/*	if (!FunctionLibrary.isElementDisplayed(By.xpath("//h[contains(text),'Add Subscriber']"), "Add Subscription header in Add subscription pop up")) {
					return "Fail : Add Subscription Button is not displayed in Alarm Subscription  Page ";
				}*/
				
                //Validate the button in Details in Add subscriber pop up
				
				if (!FunctionLibrary.isElementDisplayed(By.id("details-tab"), "Details in Add subscription pop up")) {
					return "Fail : Details  Button is not displayed in Add subscription pop up under Subscroption page ";
				}
				
				if (!FunctionLibrary.isElementDisplayed(By.id("email-subscription-tab"), "Add Subscription header in Add subscription pop up")) {
					return "Fail :Email  Button is not displayed in Add subscription pop up under Subscroption page ";
				}
				
				if (!FunctionLibrary.isElementDisplayed(By.id("sms-subscription-tab"), "Add Subscription header in Add subscription pop up")) {
					return "Fail :SMS  Button is not displayed in Add subscription pop up under Subscroption page ";
				}
				
	
				
				if (!FunctionLibrary.isElementDisplayed(By.xpath("//label[contains(text(), 'Name')]"), "Name Button in Add Subscription pop up under Subscroption page")) {
					return "Fail : Name Button is not displayed in Add  subscription pop up under Subscroption page";
				}
				
				
				if (!FunctionLibrary.isElementDisplayed(By.xpath("//label[contains(text(), 'Email')]"), "Email Button in Add Subscription pop up under Subscroption page")) {
					return "Fail : Email Button is not displayed in Add  subscription pop up under Subscroption page";
				}
				
				
				if (!FunctionLibrary.isElementDisplayed(By.xpath("//label[contains(text(), 'Mobile')]"), "Email Button in Add Subscription pop up under Subscroption page")) {
					return "Fail : Email Button is not displayed in Add  subscription pop up under Subscroption page";
				}
				
				if (!FunctionLibrary.isElementDisplayed(By.className("alert alert-info"),"Note")) {
					return "Fail : Email Button is not displayed in Add  subscription pop up under Subscroption page";
				}
				
		return "Pass: Navigated to Alarm Subscription page under Fault management Menu";
	}
	
	
	// 	// Navigate to Manager App page present under AAA menu

	public static String navigateToManagerAppPage() throws InterruptedException {
		
		// wait for page load
				FunctionLibrary.waitForPageToLoad();
						 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
				
		Thread.sleep(2000);
		//wait for element for to element
		FunctionLibrary.waitForElementToLoad(locatorConfigManagementMenu);
		
		
		//wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorAAAMenu);
		
		// Click on AAA Menu
		methodReturnResult = FunctionLibrary.clickAndWait(locatorAAAMenu, nameAAAMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		Thread.sleep(2000);
		//wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorManagerAppOption);
		
		// Click on Manager App
		methodReturnResult = FunctionLibrary.clickAndWait(locatorManagerAppOption, nameManagerAppOption);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		// Check for the Manager App Header in Manager App Page
		if(!FunctionLibrary.isElementDisplayed(locatorManagerAppHeader, nameManagerAppHeader)) {
			return "Fail : Manager App header is not displayed in Manager App";
		}
		
		// Check for the Admin Button in Manager App page
		if(!FunctionLibrary.isElementDisplayed(locatorManagerAppAdminButton, nameManagerAppAdminButton)) {
			return "Fail : Admin Button is not Displayed in Manager App";
		}
		
		//Check for the user name Search in Manager App page
		if(!FunctionLibrary.isElementDisplayed(locatorUserNameSearchInManagerApp, nameUserNameSearchInManagerApp)) {
			return "Fail : User Name Search is not displayed in Manager App";
		}
		
		// Check for the company drop down in Manager App page
		if(!FunctionLibrary.isElementDisplayed(locatorCompanyDropDn, nameCompanyDropDn)) {
			return "Fail : Company drop down is not displayed in Manager App";
		}
		
		// Check for the Role drop down in Manager App page
		if(!FunctionLibrary.isElementDisplayed(locatorRoleDropDn, nameRoleDropDn)) {
			return "Fail : Role drop down is not displayed in Manager App";
		}
		
		// Check for the Add new User in Manager App page
		if(!FunctionLibrary.isElementDisplayed(locatorAddNewUser, nameAddNewUser)) {
			return "Fail : Add New User is not displayed in Manager App";
		}
		

		return "Pass : Navigate to Manager App page under AAA menu";
	}
	
	
	// Navigate to Locations page under configmanagement menu
	
	public static String navigateToLocationsPage() throws Exception {
		
		// wait for page load
		 FunctionLibrary.waitForPageToLoad();
				 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorConfigManagementMenu);
		Thread.sleep(5000);

		// Click on Config Management Menu
		methodReturnResult = FunctionLibrary.clickLink(locatorConfigManagementMenu, nameConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
				
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorLocationOption);
		
		// click on Locations option
		methodReturnResult = FunctionLibrary.clickLink(locatorLocationOption, nameLocationOption);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// wait for page load
		 FunctionLibrary.waitForPageToLoad();
						 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorLocationsHeader);
		
		// check for Locations header in Locations Page
		if(!FunctionLibrary.isElementDisplayed(locatorLocationsHeader, nameLocationsHeader)) {
			return "Fail : Locations header is not displayed in Locations Page";
		}
		
		// Check for the Admin Button in Locations page
		if (!FunctionLibrary.isElementDisplayed(locatorAdminButtonInProfile, nameAdminButtonInProfile)) {
		     return "Fail : Admin button  is not displayed in Locations Page";
		}
		
		// Check for the company drop down in Locations page
		if(!FunctionLibrary.isElementDisplayed(locatorCompanyDropDn, nameCompanyDropDn)) {
			return "Fail : Company drop down is not displayed in Locations Page";
		}
		
		// check for the Store drop down in Locations Page
		if(!FunctionLibrary.isElementDisplayed(locatorStoreDropDn, nameStoreDropDn)) {
			return "Fail : Store drop down is not displayed in Locations Page";
		}
		
		// check for Create location button in Locations Page
		if(!FunctionLibrary.isElementDisplayed(locatorCreateLocation, nameCreateLocation)) {
			return "Fail : Create location is not displayed in Locations Page";
		}
		
		
		return "Pass : Navigate to Locations page under config management menu";
		
	}
	
	
	public static String navigateToZonesPage() throws Exception {
		
		// wait for page load
		FunctionLibrary.waitForPageToLoad();
				 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorConfigManagementMenu);
	
		// Click on Config Management Menu
		methodReturnResult = FunctionLibrary.clickLink(locatorConfigManagementMenu, nameConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorZonesOption);
		
		// click on Zones option
		methodReturnResult = FunctionLibrary.clickLink(locatorZonesOption, nameZonesOption);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		
		// wait for page load
		 FunctionLibrary.waitForPageToLoad();
						 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorZonesHeader);
		
		
		
		// check for Zone header in Zones Page
		if(!FunctionLibrary.isElementDisplayed(locatorZonesHeader, nameZonesHeader)) {
			return "Fail : Locations header is not displayed in Zones Page";
		}
		
		// Check for the Admin Button in Zones page
		if (!FunctionLibrary.isElementDisplayed(locatorAdminButtonInProfile, nameAdminButtonInProfile)) {
		     return "Fail : Admin button  is not displayed in Zones Page";
		}
		
		// Check for the company drop down in Zones page
		if(!FunctionLibrary.isElementDisplayed(locatorCompanyDropDn, nameCompanyDropDn)) {
			return "Fail : Company drop down is not displayed in Zones Page";
		}
		
		// check for the Store drop down in Zones Page
		if(!FunctionLibrary.isElementDisplayed(locatorStoreDropDn, nameStoreDropDn)) {
			return "Fail : Store drop down is not displayed in Zones Page";
		}
		
		// check for Create Zone button in Zones Page
		if(!FunctionLibrary.isElementDisplayed(locatorCreateZone, nameCreateZone)) {
			return "Fail : Create Zone is not displayed in Zones Page";
		}
		
		return "Pass : Navigate to Zones page under config management menu";
		
	}
	
	public static String navigateToCombinedLocationsPage() throws Exception {
		
		APPLICATION_LOGS.debug("Navigate To Combined Locations page under Config management menu");
		
		// wait for page load
		FunctionLibrary.waitForPageToLoad();
				 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		Thread.sleep(3000);
		
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorCombinedLocationsOption);
	
		// Click on Combined Locations page
		methodReturnResult = FunctionLibrary.clickLink(locatorCombinedLocationsOption, nameCombinedLocationsOption);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// wait for page load
		 FunctionLibrary.waitForPageToLoad();
						 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorCombinedLocationsHeader); 
		
		// check for Combined Locations header in Combined Locations Page
		if(!FunctionLibrary.isElementDisplayed(locatorCombinedLocationsHeader, nameCombinedLocationsHeader)) {
			return "Fail : Combined Locations header is not displayed in Combined Locations Page";
		}
		
		// check for create location button in Combined Locations page
		if(!FunctionLibrary.isElementDisplayed(locatorCreateCombinedLocationButton, nameCreateCombinedLocationButton)) {
			return "Fail : Create Combined Locations button is not displayed in Combined Locations Page";
		}
		
		return "Pass : Navigate to Combined Locations page under config management menu";
		
		
	}
	
	
	
	public static String navigateToNuanceGrammerPage() throws Exception {
		
		// wait for page load
				FunctionLibrary.waitForPageToLoad();
						 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
				
		Thread.sleep(2000);
		//wait for element for to element
		FunctionLibrary.waitForElementToLoad(locatorConfigManagementMenu);

		// Click on Config Management Menu
		methodReturnResult = FunctionLibrary.clickLink(locatorConfigManagementMenu, nameConfigManagementMenu);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorGrammer);
		
		// click on Grammer menu under config management
		methodReturnResult = FunctionLibrary.clickLink(locatorGrammer, nameGrammer);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		Thread.sleep(2000);
		// wait for element to load
		FunctionLibrary.waitForElementToLoad(locatorNuanceGrammer);
		
		// click on nuance grammer
		methodReturnResult = FunctionLibrary.clickLink(locatorNuanceGrammer, nameNuanceGrammer);
		if (methodReturnResult.contains("Fail")) {
			return methodReturnResult;
		}
		
		// wait for page load
		FunctionLibrary.waitForPageToLoad();
								 
		// wait for element to disappear
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
		
		// verify Nuance grammer Header in Nuance Grammer page
		if(!FunctionLibrary.isElementDisplayed(locatorNuanceGrammerHeader, nameNuanceGrammerHeader)) {
			return "Fail : Nuance Grammer header is not present in Nuance Grammer page";
		}
		
		// Check for the Admin Button in Nuance Grammer page
		if (!FunctionLibrary.isElementDisplayed(locatorAdminButtonInProfile, nameAdminButtonInProfile)) {
		     return "Fail : Admin button  is not displayed in Nuance Grammer Page";
		}
				
		// Check for the company drop down in Nuance Grammer page
		if(!FunctionLibrary.isElementDisplayed(locatorCompanyDropDn, nameCompanyDropDn)) {
			return "Fail : Company drop down is not displayed in Nuance Grammer Page";
		}
				
		// check for the Store drop down in Nuance Grammer Page
		if(!FunctionLibrary.isElementDisplayed(locatorStoreDropDn, nameStoreDropDn)) {
			return "Fail : Store drop down is not displayed in Nuance Grammer Page";
		}
		
		// check for Id is present in Nuance Grammer page
		if(!FunctionLibrary.isElementDisplayed(locatorNuanceId, nameNuanceId)) {
			return "Fail : Id is not present in Nuance Grammer Page";
		}
		
		// check for Command is present in Nuance Grammer page
		if(!FunctionLibrary.isElementDisplayed(locatorNuanceCommand, nameNuanceCommand)) {
			return "Fail : Command is not present in Nuance Grammer Page";
		}
		
		// check for tagout is present in Nuance grammer page
		if(!FunctionLibrary.isElementDisplayed(locatorNuanceTagout, nameNuanceTagout)) {
			return "Fail : Tagout is not present in Nuance Grammer page";
		}
		
		// check for Aliases is present in Nuance Grammer page
		if(!FunctionLibrary.isElementDisplayed(locatorNuanceAliases, nameNuanceAliases)) {
			return "Fail : Aliases is not present in Nuance Grammer page";
		}
		
		// check for Targets is present in Nuance Grammer page
		if(!FunctionLibrary.isElementDisplayed(locatorNuanceTargets, nameNuanceTargets)) {
			return "Fail : Targets is not Present in Nuance Grammer page";
		}
		
		// check for save is present in Nuance Grammer page
		if(!FunctionLibrary.isElementDisplayed(locatorNuanceSave, nameNuanceSave)) {
			return "Fail : save tag is not present in Nuance Grammer page";
		}
		
		return "Pass : Navigate to Nuance Grammer under config management menu";
		
	}
	
}
