/*
 * Keyword.java contains all the keywords that has been developed. Keyword in Keyword.java file and one in Controller.xls sheet should match  
 */
package testscripts;

import java.io.FileNotFoundException;
import java.io.IOException;

import jxl.read.biff.BiffException;

import testcases.ManagerAppLoginAPI;

import testcases.StoreCreate;


public class Keyword {
		
		/*****************************
		 * MA login API
		 * 
		 * @throws Exception
		 *****************************************/
		//Delete Zone and locations in zone and location page
		public static String maLogin() throws Exception {
			return ManagerAppLoginAPI.maLogin();
		}
		
		
		
		
		//store create api
		public static String createStoreAPI() throws Exception {
			return StoreCreate.createStoreAPI();
		}
		
		
		
		
		
		

}
