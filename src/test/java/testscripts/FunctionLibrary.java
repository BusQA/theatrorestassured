/*
 * FunctionLibrary.java contains all the reusable functions needed for the test  
 */
package testscripts;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FunctionLibrary extends DriverScript {
	/**
	 * public static String closeDriver() method specification :-
	 * 
	 * 1) Closes the web driver 2) driver.close() -> Closes the webdriver
	 * 
	 * @param :
	 *            no parameters
	 * 
	 * @return : Result of execution - Pass or fail (with cause)
	 */

	public static String closeDriver() throws InterruptedException {

		APPLICATION_LOGS.debug("Closing the driver ...");

		try {

			// Close the driver
			// driver.close();
			driver.quit();

			// Make driver to point to null
			wbdv = null;

			// Log result
			APPLICATION_LOGS.debug("Closed the driver");

			return "Pass : Closed the driver";

		}

		catch (Throwable closeDriverException) {

			// Log error
			APPLICATION_LOGS.debug("Error came while closing driver : " + closeDriverException.getMessage());

			return "Fail : Error came while closing driver : " + closeDriverException.getMessage();

		}

	}

	/**
	 * public static Boolean isElementDisplayed(By locator, String elemName)
	 * method specification :-
	 * 
	 * driver.findElement(locator).isDisplayed() : Verifying whether element
	 * displayed or not
	 * 
	 * @param :
	 *            web element locator, web element name
	 * 
	 * @return : (true) - If element is displayed (false) - If element not
	 *         displayed
	 */
	public static Boolean isElementDisplayed(By locator, String elemName) {

		APPLICATION_LOGS.debug("Checking whether " + elemName + " is displayed on the page or not ...");

		Boolean isDisplayed;

		try {

			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			// Check whether web element is displayed or not
			isDisplayed = driver.findElement(locator).isDisplayed();

			if (isDisplayed)
				APPLICATION_LOGS.debug(elemName + " is displayed on the page");

			else
				APPLICATION_LOGS.debug(elemName + " not displayed on the page");

			return isDisplayed;

		}

		catch (NoSuchElementException elementPresentError) {

			APPLICATION_LOGS.debug(elemName + " not present on the page");
			return false;

		}

	}

	/**
	 * 
	 * public static String input(By locator,String elemName,String Value)
	 * method specification :-
	 * 
	 * 1) locator -> identify the web element by id,x-path,name,etc. 2) elemName
	 * -> the name of the web element where we intend to input/send values 3)
	 * Value -> the string value which we intend to input/send 4)
	 * driver.findElement(locator).sendKeys(Value) -> inputs/sends the value to
	 * the intended web element
	 * 
	 * @param :
	 *            Locator for the input-box, name of the web element, value to
	 *            input
	 * 
	 * @return : Result of execution - Pass or fail (with cause)
	 */

	public static String input(By locator, String elemName, String Value) {

		try {

			// Send values to the input box
			APPLICATION_LOGS.debug("Sending Values in : " + elemName);
			driver.findElement(locator).sendKeys(Value);

			// Log result
			APPLICATION_LOGS.debug("Wrote '" + Value + "' text into : '" + elemName + "'");

			return "Pass : Wrote '" + Value + "' text into : '" + elemName + "'";

		}

		catch (Throwable inputException) {

			// Log error
			APPLICATION_LOGS.debug("Error while inputting into - '" + elemName + "' : " + inputException.getMessage());

			return "Fail : Error while inputting into - '" + elemName + "' : " + inputException.getMessage();

		}

	}

	/**
	 * Waits for element to appear on the page. Once appeared, highlight the
	 * element and clicks on it. Returns Pass if able to click on the element.
	 * Returns Fail if any exception occurs in between.
	 * 
	 * @param locator
	 *            Element locator
	 * @param elemName
	 *            Element name
	 * 
	 * @return Pass/Fail
	 */

	public static String clickLink(By locator, String elemName) {

		APPLICATION_LOGS.debug("Clicking on : " + elemName);

		try {

			// Click on the child element which is under parent element
			driver.findElement(locator).click();
			
			// Log result
			APPLICATION_LOGS.debug("Clicked on : " + elemName);

			return "Pass : Clicked on : " + elemName;

		}

		catch (Throwable clickLinkException) {

			// Log error
			APPLICATION_LOGS.debug("Error while clicking on - " + elemName + " : " + clickLinkException.getMessage());

			return "Fail : Error while clicking on - " + elemName + " : " + clickLinkException.getMessage();

		}

	}

	/**
	 * Waits for input box to appear on the page. Once appeared, highlight and
	 * clears the box. Returns Pass if Input box got cleared successfully.
	 * Returns Fail if input box didn't clear or any exception occurs in
	 * between.
	 * 
	 * @param locator
	 *            Element locator
	 * @param elemName
	 *            Element name
	 * 
	 * @return Pass/Fail
	 */

	public static String clearField(By locator, String elemName) {

		APPLICATION_LOGS.debug("Clearing field : " + elemName);

		try {

			// Clear the input-box
			driver.findElement(locator).clear();

			// Check whether input-box has been cleared or not
			if (!driver.findElement(locator).getAttribute("value").isEmpty()) 
				driver.findElement(locator).clear();

			// Log result
			APPLICATION_LOGS.debug("Cleared : " + elemName);
			
			return "Pass : Cleared : " + elemName;

		}

		catch (Throwable clearFieldException) {

			// Log error
			APPLICATION_LOGS.debug("Error while clearing - " + elemName + " : " + clearFieldException.getMessage());

			return "Fail : Error while clearing - " + elemName + " : " + clearFieldException.getMessage();

		}

	}

	// Generate randomn characters
	public static String randomnGenerator(int length) {

		String randomnCharacters;
		StringBuffer sb = new StringBuffer();

		for (int x = 0; x < length; x++) {

			sb.append((char) ((int) (Math.random() * 26) + 97));

		}

		randomnCharacters = sb.toString();
		return randomnCharacters;

	}

	/**
	 * public static String assertText(String elemName,String actValue, String
	 * expValue) method specification :-
	 * 
	 * 1) Verifies and returns TRUE if expected and actual text match 2)
	 * elemName -> the name/type of text we intend to compare 3) actValue -> the
	 * actual string value which is shown in the application 4) expValue -> the
	 * expected string value which should be shown in the application
	 * 
	 * @param :
	 *            Name of the web element, Actual text and expected text
	 * 
	 * @return : Result of execution - Pass or fail (with cause)
	 */

	public static String assertText(String elemName, String actValue, String expValue) {

		APPLICATION_LOGS.debug("Asserting text for : '" + elemName + "' where Expected text is '" + expValue
				+ "' and Actual text is '" + actValue + "'");

		try {

			// Assert that expected value matches with actual value
			Assert.assertEquals(expValue.trim(), actValue.trim());

			// Log result
			APPLICATION_LOGS.debug("Successfully asserted text for : '" + elemName + "' where Expected text is '"
					+ expValue + "' and Actual text is '" + actValue + "'");

			return "Pass : Expected text matches with actual text";

		}

		catch (Throwable assertTextException) {

			// Log error
			APPLICATION_LOGS
					.debug("Error while Asserting Text for - '" + elemName + "' : " + assertTextException.getMessage());

			return "Fail : Error while Asserting Text for - '" + elemName + "' : " + assertTextException.getMessage();

		}

	}

	/*
	 * public static void clickAndWait(By locator,String elemName) method
	 * specification :-
	 * 
	 * 1) Click and wait for next page to load 2)
	 * driver.findElement(locator).click() -> Clicks on the web element targeted
	 * by locator
	 * 
	 * @param : Locator to locate the web element, Name of the web element
	 * 
	 * @return : Result of execution - Pass or fail (with cause)
	 */

	public static String clickAndWait(By locator, String elemName) {

		try {

			// Click on the web element targeted by locator
			methodReturnResult = FunctionLibrary.clickLink(locator, elemName);
			if (methodReturnResult.contains("Fail"))
				return methodReturnResult;

			// Wait for new page to load
			FunctionLibrary.waitForPageToLoad();

			// Log result
			APPLICATION_LOGS.debug(
					"Clicked on the element : " + elemName + " and new page loaded with title : " + driver.getTitle());

			return "Pass : Clicked on the element : " + elemName + " and new page loaded with title : "
					+ driver.getTitle();

		}

		catch (Throwable clickAndWaitException) {

			// Log error
			APPLICATION_LOGS.debug("Error while clicking on " + elemName + " and waiting for new page to load : "
					+ clickAndWaitException.getMessage());

			return "Error while clicking on link " + elemName + " and waiting for new page to load : "
					+ clickAndWaitException.getMessage();

		}

	}
	/*
	 * public static void waitForPageToLoad() method specification :-
	 * 
	 * 1) Waits for a new page to load completely 2) new WebDriverWait(driver,
	 * 60) -> Waits for 60 seconds 3) wait.until((ExpectedCondition<Boolean>) ->
	 * Wait until expected condition (All documents present on the page get
	 * ready) met
	 * 
	 * @param : no parameters passed
	 * 
	 * @return : void
	 */

	public static void waitForPageToLoad() throws InterruptedException {

		try {

			// Waits for 60 seconds
			WebDriverWait wait = new WebDriverWait(driver, 60);
			// Wait until expected condition (All documents present on the page
			// get ready) met
			wait.until((ExpectedCondition<Boolean>) new ExpectedCondition<Boolean>() {

				public Boolean apply(WebDriver d) {

					if (!(d instanceof JavascriptExecutor))
						return true;

					Object result = ((JavascriptExecutor) d)
							.executeScript("return document['readyState'] ? 'complete' == document.readyState : true");

					if (result != null && result instanceof Boolean && (Boolean) result)
						return true;

					return false;

				}

			});

		}

		catch (Throwable waitForPageToLoadException) {
			APPLICATION_LOGS
					.debug("Error came while waiting for page to load : " + waitForPageToLoadException.getMessage());
		}

	}

	/*
	 * public static void waitForElementToDisappear(By locator) method
	 * specification :-
	 * 
	 * 1) Waits for the web element to appear on the page 2) new
	 * WebDriverWait(driver, 60) -> Waits for 60 seconds 3)
	 * wait.until((ExpectedCondition<Boolean>) -> Wait until expected condition
	 * (All documents present on the page get ready) met
	 * 
	 * @param : locator for the web element
	 * 
	 * @return : void
	 */

	public static void waitForElementToDisappear(final By locator, String elemName) {

		APPLICATION_LOGS.debug("Waiting for " + elemName + " to disappear ...");

		try {

			// Waits for 60 seconds
			Wait<WebDriver> wait = new WebDriverWait(driver, 10);

			// Wait until the element get disappeared

			@SuppressWarnings("unused")
			WebElement element = wait.until(ElementLocatedToGetDisappear(locator));

			// Log result
			APPLICATION_LOGS.debug("Waiting ends ... " + elemName + " disappeared");

		}

		catch (Throwable waitForElementException) {

			// Log error
			APPLICATION_LOGS.debug(
					"Error came while waiting for element to disappear : " + waitForElementException.getMessage());

		}

	}

	/*
	 * public static ExpectedCondition<WebElement>
	 * ElementLocatedToGetDisappear(final By locator) method specification :-
	 * 
	 * 1) Waits for the web element to disappear on the page 2) WebElement
	 * toReturn.isDisplayed() -> Returns true if displayed on the page, else
	 * returns false
	 * 
	 * @param : Locator to locate the web element
	 * 
	 * @return : ExpectedCondition about the web element
	 */

	public static ExpectedCondition<WebElement> ElementLocatedToGetDisappear(final By locator) {

		return new ExpectedCondition<WebElement>() {

			public WebElement apply(WebDriver driver) {

				// Store the web element
				WebElement toReturn = driver.findElement(locator);

				// Check whether the web element is disappeared
				if (!toReturn.isDisplayed())
					return toReturn;

				return null;

			}

		};

	}

	/*
	 * public static String selectValueByVisibleText(By Locator, String Option,
	 * String elemName) method specification :-
	 * 
	 * 1) Select value from drop-down by visible text 2) Select -> This is a
	 * in-built class in Selenium which is used to represent a drop-down 3)
	 * select.selectByVisibleText(Value) -> Select by visible text
	 * 
	 * @param : Locator for the drop-down field, Option to be selected, Name of
	 * the web element
	 * 
	 * @return : Result of execution - Pass or fail (with cause)
	 */

	public static String selectValueByVisibleText(By Locator, String Option, String elemName) {

		APPLICATION_LOGS.debug("Selecting '" + Option + "' from : " + elemName);

		try {

			// Locate drop-down field
			Select select = new Select(driver.findElement(Locator));

			// Select value from drop-down
			select.selectByVisibleText(Option);

			// Log result
			APPLICATION_LOGS.debug("Selected '" + Option + "' from : " + elemName);

			return "Pass : Selected '" + Option + "' from : " + elemName;

		}

		catch (Throwable selectValueException) {

			// Log error
			APPLICATION_LOGS.debug(
					"Error while Selecting Value from - '" + elemName + "' : " + selectValueException.getMessage());

			return "Fail : Error while Selecting Value from - '" + elemName + "' : "
					+ selectValueException.getMessage();

		}

	}

	/*
	 * public static Boolean isElementEnabled(By locator, String elemName)
	 * method specification :-
	 * 
	 * driver.findElement(locator).isEnabled() : Verifying whether element
	 * enabled or not
	 * 
	 * @param : web element locator, web element name
	 * 
	 * @return : (true) - If element is enabled (false) - If element not enabled
	 */
	public static Boolean isElementEnabled(By locator, String elemName) {

		APPLICATION_LOGS.debug("Checking whether " + elemName + " is enabled on the page or not ...");

		Boolean isEnabled;

		try {

			// Check whether web element is enabled or not
			isEnabled = driver.findElement(locator).isEnabled();

			if (isEnabled)
				APPLICATION_LOGS.debug(elemName + " is enabled on the page");

			else
				APPLICATION_LOGS.debug(elemName + " not enabled on the page");

			return isEnabled;

		}

		catch (NoSuchElementException elementPresentError) {

			APPLICATION_LOGS.debug(elemName + " not present on the page");
			return false;

		}

	}

	/*
	 * public static void isChecked(By locator, String elemName) method
	 * specification :-
	 * 
	 * 1) Verifies whether a Checkbox is checked or not 2) locator -> to locate
	 * the element 3) elemName -> the name/type of the check-box which we intend
	 * to check 4) driver.findElement(locator).isSelected() -> is to verify
	 * whether the intended element is checked or not
	 * 
	 * @param : Locator for the Check-box, name of the web element
	 * 
	 * @return : Result of execution - Pass or fail (with cause)
	 */

	public static String isChecked(By locator, String elemName) {

		APPLICATION_LOGS.debug("Verifying is the checkbox checked : " + elemName);

		String result = null;

		try {

			// Verify whether check-box if already checked
			if (driver.findElement(locator).isSelected()) {

				// Log the result
				APPLICATION_LOGS.debug("'" + elemName + "' is checked");
				result = "Pass : '" + elemName + "' is checked";

			}

			else {

				// Log the result
				APPLICATION_LOGS.debug("'" + elemName + "' is not checked");
				result = "Fail : '" + elemName + "' is not checked";

			}

		}

		catch (Throwable ischeckCheckBoxException) {

			// Log the exception
			APPLICATION_LOGS.debug("Error while verifying checkbox is checked '" + elemName + "' : "
					+ ischeckCheckBoxException.getMessage());

			result = "Error while verifying checkbox is checked: '" + elemName + "' : "
					+ ischeckCheckBoxException.getMessage();

		}

		return result;

	}

	/*
	 * public static void waitForElementToLoad(By locator) method specification
	 * :-
	 * 
	 * 1) Waits for the web element to appear on the page 2) new
	 * WebDriverWait(driver, 60) -> Waits for 60 seconds 3)
	 * wait.until((ExpectedCondition<Boolean>) -> Wait until expected condition
	 * (All documents present on the page get ready) met
	 * 
	 * @param : no parameters passed
	 * 
	 * @return : void
	 */

	public static void waitForElementToLoad(final By locator) {

		APPLICATION_LOGS.debug("Waiting for web element to load on the page");

		try {

			// Waits for 60 seconds
			Wait<WebDriver> wait = new WebDriverWait(driver, 60);

			// Wait until the element is located on the page
			@SuppressWarnings("unused")
			WebElement element = wait.until(visibilityOfElementLocated(locator));

			// Log result
			APPLICATION_LOGS.debug("Waiting ends ... Web element loaded on the page");

		}

		catch (Throwable waitForElementException) {

			// Log error
			APPLICATION_LOGS
					.debug("Error came while waiting for element to appear : " + waitForElementException.getMessage());

		}

	}

	/*
	 * public static ExpectedCondition<WebElement>
	 * visibilityOfElementLocated(final By locator) method specification :-
	 * 
	 * 1) Waits for the web element to appear on the page 2) WebElement
	 * toReturn.isDisplayed() -> Returns true if displayed on the page, else
	 * returns false
	 * 
	 * @param : Locator to locate the web element
	 * 
	 * @return : ExpectedCondition about the web element
	 */

	public static ExpectedCondition<WebElement> visibilityOfElementLocated(final By locator) {

		return new ExpectedCondition<WebElement>() {

			public WebElement apply(WebDriver driver) {

				// Store the web element
				WebElement toReturn = driver.findElement(locator);

				// Check whether the web element is displayed on the page
				if (toReturn.isDisplayed())
					return toReturn;

				return null;

			}

		};

	}

	public static <T extends Comparable<? super T>> boolean isSorted(Iterable<T> iterable) {
		Iterator<T> iter = iterable.iterator();
		if (!iter.hasNext()) {
			return true;
		}
		T t = iter.next();
		while (iter.hasNext()) {
			T t2 = iter.next();
			if (t.compareTo(t2) > 0) {
				return false;
			}
			t = t2;
		}
		return true;
	}

	/*
	 * public static String assertAlertAndAccept(String expectedAlertText)
	 * method specification :-
	 * 
	 * driver.switchTo().alert() -> Switch to the alert appeared on the page 3)
	 * Assert.assertEquals() -> Asserts for equality 4) alert.accept() ->
	 * Accepts the alert
	 * 
	 * @param : Expected alert text to assert
	 * 
	 * @return : Result of execution - Pass or fail (with cause)
	 */

	public static String assertAlertAndAccept(String expectedAlertText) {

		APPLICATION_LOGS.debug("Asserting alert text : " + expectedAlertText);

		String actualAlertText = null;
		Alert alert = null;

		try {

			// Switch control to alert
			alert = driver.switchTo().alert();

			// Get the actual alert message
			actualAlertText = alert.getText();

			// Assert alert message
			Assert.assertEquals(expectedAlertText.trim(),
					actualAlertText.trim());
			Thread.sleep(3000L);

			// Accept alert message
			alert.accept();
			Thread.sleep(3000L);

			// log result
			APPLICATION_LOGS.debug("Success : got the alert message saying : "
					+ actualAlertText);

			return "Pass : got the alert message saying : '" + actualAlertText;

		}

		catch (Throwable alertExcpetion) {

			APPLICATION_LOGS
					.debug("Error came while asserting alert and accepting : "
							+ alertExcpetion.getMessage());
			return "Fail : Error came while asserting alert and accepting : "
					+ alertExcpetion.getMessage();

		}

	}
	
	/*
	 * public static Boolean isElementPresent(By locator, String elemName)
	 * method specification :-
	 * 
	 * driver.findElement(locator) : Checking whether element present or not
	 * 
	 * @param : web element locator, web element name
	 * 
	 * @return : (true) - If element is present (false) - If element not present
	 */
	public static Boolean isElementPresent(By locator, String elemName) {

		APPLICATION_LOGS.debug("Checking whether " + elemName
				+ " is present on the page or not ...");

		try {

			// Check whether web element is displayed or not
			driver.findElement(locator);

			APPLICATION_LOGS.debug(elemName + " is present on the page");
			return true;

		}

		catch (NoSuchElementException elementPresentError) {

			APPLICATION_LOGS.debug(elemName + " not present on the page");
			return false;

		}

	}
	
	/*
	 * public static String checkCheckBox(By locator, String elemName) method
	 * specification :
	 * 
	 * 1) Checks a check-box if it is not checked already 2) if
	 * (!driver.findElement(locator).isSelected()) {
	 * driver.findElement(locator).click() : Checks the checkbox if it is not
	 * checked already 3) String elemName : Passed as a parameter to name the
	 * element
	 */
	public static String checkCheckBox(By locator, String elemName) {

		APPLICATION_LOGS.debug("Checking : " + elemName);

		try {

			// Wait for element to load
			waitForElementToLoad(locator);

			// Select the element if not selected already
			if (!driver.findElement(locator).isSelected()) {

				driver.findElement(locator).click();

				APPLICATION_LOGS.debug("Checked " + elemName);
				return "Pass : Checked " + elemName;

			}

			else {

				APPLICATION_LOGS.debug(elemName + " is already checked");

				return "Pass : " + elemName + " is already checked";

			}

		}

		catch (Throwable checkCheckBoxException) {

			APPLICATION_LOGS.debug("Error while checking -" + elemName
					+ checkCheckBoxException.getMessage());
			return "Fail : Error while checking -" + elemName
					+ checkCheckBoxException.getMessage();

		}

	}
	
}
