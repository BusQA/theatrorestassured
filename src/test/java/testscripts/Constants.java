/*
 * Constants.java contains all the constants need for the test  
 */
package testscripts;

public class Constants {
	public static final String noUserFoundAlert = "User not found";
	public static final String forgotPasswordMailTriggerAlert = "A link has been sent to your registered mail id for creating new password";
	public static final String alertForUserPassword = "Password has been used by you,Please choose different Password";
	public static final String companyPageHeader = "Welcome Company";
	public static final String companyNamePlaceholderText = "<Ex:CompanyName>";
	public static final String nickNamePlaceholderText = "<Ex:NickName>";
	public static final String address1PlaceholderText = "<Street Name>";
	public static final String address2PlaceholderText = "<Addresss Line2>";
	public static final String cityPlaceholderText = "<City>";
	public static final String zipPlaceholderText = "<Postal/ZipCode>";
	public static final String primaryContactNamePlaceholderText = "<Primary Contact Name>";
	public static final String primaryMailIDPlaceholderText = "<Primary Contact Email>";
	public static final String primaryContactNumberPlaceholderText = "<(999) 999-9999>";
	public static final String technicalNamePlaceholderText = "<Technical Contact Name>";
	public static final String technicalMailIDPlaceholderText = "<Tecnical Contact Email>";
	public static final String technicalNumberPlaceholderText = "(999) 999-9999";
	public static final String additionalInstructionPlaceholderText = "<Additional Instructions>";
	public static final String storePageHeader = "Stores";
	public static final String employeePageHeader = "Employees";
	public static final String storeDetailsPageHeader = "Store Details";
	public static final String fieldRequiredClass = "field_required";
	public static final String companyNameErrorMsg = "Company name cannot be empty";
	public static final String companyNickNameErrorMsg = "Company nickName cannot be empty";
	public static final String primaryNameErrorMsg = "Primary name should not be null.";
	public static final String primaryNameLengthErrorMsg = "Primary name should be 1 to 100 characters.";
	public static final String primaryContactNumberLengthErrorMsg = "Primary phone number length should be 10 to 15 digits.";
	public static final String primaryContactNumberErrorMsg = "Primary phone number should not be null.";
	public static final String technicalNameErrorMsg = "Technical name cannot be empty";
	public static final String technicalMailIDErrorMsg = "Technical email id should not empty.";
	public static final String technicalContactNumberErrorMsg = "Technical phone number should not be null.";
	public static final String technicalContactNumberLengthErrorMsg = "Technical phone number length should be 10 to 15 digits.";
	public static final String storeNameErrorMsg = "Please enter a Store Name.";
	public static final String unsavedChagesPopup = "You have unsaved changes. If you leave now, you will lose these changes. Do you really want to leave?";
	public static final String employeeFirstNameErrorMsg = "Please enter First Name.";
	public static final String employeeLastNameErrorMsg = "Please enter Last Name.";
	public static final String employeeGrammarPageHeader = "Employee Grammar";
	public static final String groupPageHeader = "Groups";
	public static final String groupNameErrorMessage = "Please enter Group Name.";
	public static final String employeeHeaderText = "Employees";
	public static final String primaryEmailErrorMsg = "Primary email id should not be null.";
	public static final String technicalNameLengthErrorMsg = "Technical name should be 1 to 100 characters.";
	public static final String locationErrorMessage = "Please enter a location name";
	
}
