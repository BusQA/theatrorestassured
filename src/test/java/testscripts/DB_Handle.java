package testscripts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DB_Handle extends DriverScript {

	public static void getStoreIDBeforeCallingAPI() throws Exception {
		
		APPLICATION_LOGS.debug("Retrieving StoreID before calling Store Creation API");
		String data = null;
		
		Connection connection = null;
		try {
			  // Load the MySQL JDBC driver
			  String driverName = "com.mysql.jdbc.Driver";
			  Class.forName(driverName);
			  
			  String dburl = testData.getCellData("DB_Handle", "URL", 1);
			// Create a connection to the database
			  String serverName = dburl;
			  String schema = "theatrobiz";
			  String url = "jdbc:mysql://" + serverName +  "/" + schema;
			  String username = "theatro";
			  String password = "theatro";
			  System.out.println(url);
			  connection = DriverManager.getConnection(url, username, password);
			 
			  System.out.println("Successfully Connected to the database!");  
		}
		
		catch (ClassNotFoundException e) {
		 
		  System.out.println("Could not find the database driver " + e.getMessage());
		 } 
		catch (SQLException e) {
		 
		  System.out.println("Could not connect to the database " + e.getMessage());
		}
		
		
		try {
			 
			 
			// Get a result set containing all data from test_table
			 
			Statement statement = connection.createStatement();
			 
			ResultSet results = statement.executeQuery("select max(StoreID) from stores");
			 
			// For each row of the result set ...
			 
			while (results.next()) {
			 
			 
			  // Get the data from the current row using the column index - column data are in the VARCHAR format
			  data = results.getString(1);
			 
			  System.out.println("Fetching data by column index for row " + results.getRow() + " : " + data);
			 
			 
			   // Get the data from the current row using the column name - column data are in the VARCHAR format
			  /*data = results.getString("max(StoreID)");
			  System.out.println("Fetching data by column name for row " + results.getRow() + " : " + data);*/
			 
			  testData.setCellData("DB_Handle", "StartStoreID", 1, data);
			 
			}
			
		}
		catch (SQLException e) {
			 
			  System.out.println("Could not retrieve data from the database " + e.getMessage());
		} 
	}
	
	public static void getStoreIDAfterCallingAPI() throws Exception {
		
		APPLICATION_LOGS.debug("Retrieving StoreID after calling ServerConfiguration Creation API");
		String data = null;
		
		Connection connection = null;
		try {
			  // Load the MySQL JDBC driver
			  String driverName = "com.mysql.jdbc.Driver";
			  Class.forName(driverName);
			  
			  String dburl = testData.getCellData("DB_Handle", "URL", 1);
			// Create a connection to the database
			  String serverName = dburl;
			  String schema = "theatrobiz";
			  String url = "jdbc:mysql://" + serverName +  "/" + schema;
			  String username = "theatro";
			  String password = "theatro";
			  connection = DriverManager.getConnection(url, username, password);
			 
			   
			 
			  System.out.println("Successfully Connected to the database!");  
		}
		
		catch (ClassNotFoundException e) {
		 
		  System.out.println("Could not find the database driver " + e.getMessage());
		 } 
		catch (SQLException e) {
		 
		  System.out.println("Could not connect to the database " + e.getMessage());
		}
		
		
		try {
			 
			 
			// Get a result set containing all data from test_table
			 
			Statement statement = connection.createStatement();
			 
			
			ResultSet results = statement.executeQuery("select max(StoreID) from stores");
			 
			// For each row of the result set ...
			 
			while (results.next()) {
			 
			 
			  // Get the data from the current row using the column index - column data are in the VARCHAR format
			  data = results.getString(1);
			 
			  System.out.println("Fetching data by column index for row " + results.getRow() + " : " + data);
			 
			 
			   // Get the data from the current row using the column name - column data are in the VARCHAR format
			  /*data = results.getString("max(StoreID)");
			  System.out.println("Fetching data by column name for row " + results.getRow() + " : " + data);*/
			 
			  testData.setCellData("DB_Handle", "EndStoreID", 1, data);
			 
			}
			
		}
		catch (SQLException e) {
			 
			  System.out.println("Could not retrieve data from the database " + e.getMessage());
		} 
		
	}
}
