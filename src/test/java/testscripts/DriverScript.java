/*
 * Driverscript.java is the driving unit of test and is developed using testNG
 */
package testscripts;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.Logger;
//import org.junit.AfterClass;
//import org.junit.BeforeClass;
//import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import datatable.XlsReader;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;
import jxl.read.biff.BiffException;
import reports.ReportUtil;
import util.TestUtil;

public class DriverScript {
	public static Properties CONFIG;
	public static Properties LOG;
	public static XlsReader controller;
	public static XlsReader testData;
	public static String firstSheetName;
	public static String currentTest;
	public static String keyword;
	public static String currentTCID;
	public static ResponseSpecification checkStatusCodeAndContentType;
	public static String stepDescription;
	public static WebDriver wbdv = null;
	public static EventFiringWebDriver driver = null;
	public static ArrayList<String> testStatus = new ArrayList<String>();
	public static DesiredCapabilities dc = null;
	public static String methodReturnResult = null;
	public static Logger APPLICATION_LOGS = Logger.getLogger("devpinoyLogger");
	public static Connection con;
	public static FileOutputStream fos;
	public static String screenshotPath = System.getProperty("user.dir") + "/Report/";
	
	// Database connection
	public static String sqlhostname = null;
	public static String sqlport = null;
	public static String sqlsid = null;
	public static String sqlconnectionString = null;
	public static String sqldbUsername = null;
	public static String sqldbPassword = null;
	public static String connectionString = null;

	/* ................. Locators for the test ............... */
	public static By locatorAdminDropdown = By.id("user-dropdown");
	public static By locatorLogoutLink = By.linkText("Log Out");

	/* ................. Names of locators for the test ............... */
	public static String nameAdminDropdown = "'Admin dropdown' in Theatro home page";
	public static String nameLogoutLink = "'Logout link' in Theatro home page";


	/*static ProxyServer server = new ProxyServer(4446);*/
	
	@BeforeClass
	public static void initialize() throws Exception {

		// Override default J2SE built-in workable logger built-in
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");

		// Locates and loads the config properties
		CONFIG = new Properties();
		FileInputStream fs = new FileInputStream(
				System.getProperty("user.dir") + "/src/test/java/config/config.properties");
		CONFIG.load(fs);

		// Locate Application Log
		LOG = new Properties();
		fs = new FileInputStream(System.getProperty("user.dir") + "/src/test/java/Log4j.properties");
		LOG.load(fs);
		LOG.setProperty("log4j.appender.dest1.File",
				System.getProperty("user.dir") + "/src/test/java/config/application.log");
		LOG.store(new FileOutputStream(System.getProperty("user.dir") + "/src/test/java/Log4j.properties"), null);

		// Locates controller sheet
		controller = new XlsReader(System.getProperty("user.dir") + "/src/test/java/config/controller.xls");

		// Locates testData sheet
		testData = new XlsReader(System.getProperty("user.dir") + "/src/test/java/config/testData.xls");

		// Start the process of HTML report generation
		ReportUtil.startTesting(System.getProperty("user.dir") + "/Report/index.html", CONFIG.getProperty("env"),
				CONFIG.getProperty("version"), CONFIG.getProperty("test_browser"),
				CONFIG.getProperty("theatroCentralURL"));
		

		checkStatusCodeAndContentType = 
		    new ResponseSpecBuilder().
		        expectStatusCode(200).
		        expectContentType(ContentType.JSON).expectHeader("server", "nginx").
		        build();
		
	}

	/*@Before
	// Connects to database
	public void databaseConnectivity() throws BiffException, IOException {

		// Navigate to Discharge Central, Login and Select the hospital from
		// dropdown
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			APPLICATION_LOGS.debug("Unable to locate jdbc driver");
			e.printStackTrace();
		}

		try {

			// Get the hostname
			sqlhostname = testData.getCellData("DB_Info", "Hostname", 1);

			// Get the port
			sqlport = testData.getCellData("DB_Info", "Port", 1);

			// Get the username
			sqldbUsername = testData.getCellData("DB_Info", "Username", 1);

			// Get the password
			sqldbPassword = testData.getCellData("DB_Info", "Password", 1);

			// Make connection string
			connectionString = "jdbc:mysql://itg.theatro.com:3306/theatrobiz?allowMultiQueries=true";
			con = DriverManager.getConnection(connectionString, "theatro", "theatro");

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (con != null) {
			APPLICATION_LOGS.debug("Mysql connection with jdbc is established");
		} else {
			APPLICATION_LOGS.debug("Failed to connect to mysql");
		}

	}*/

	@Test
	public void testApp() throws IOException, BiffException, InterruptedException {

		// Get the first sheet name under 'controller.xls'
		firstSheetName = controller.getFirstSheetname();
		ReportUtil.startSuite(firstSheetName);
		for (int tsid = 1; tsid < controller.getRowCount(firstSheetName); tsid++) {

			
			// Stores the current sub-module
			currentTest = controller.getCellData(firstSheetName, "TSID", tsid).trim();

			// Runs the respective sub-module if Runmode for the
			// sub-module is
			// 'Y'
			if (controller.getCellData(firstSheetName, "Runmode", tsid).equals("Y")) {
				APPLICATION_LOGS.debug("Executing test : " + currentTest);

				// Implement keyword
				for (int tcid = 1; tcid < controller.getRowCount(currentTest); tcid++) {
					StopWatch stopwatch = new StopWatch();
					stopwatch.reset();
					stopwatch.start();
					// values from xls
					// Stores the current keyword
					keyword = controller.getCellData(currentTest, "Keyword", tcid).trim();

					// Stores the current TSID
					currentTCID = controller.getCellData(currentTest, "TCID", tcid).trim();

					// Stores the current description
					stepDescription = controller.getCellData(currentTest, "Description", tcid).trim();

					try {

						Method method = Keyword.class.getMethod(keyword);
						String result = (String) method.invoke(method);
						APPLICATION_LOGS.debug("Result of test case execution - " + result);

						if (!result.startsWith("Fail")) {
							stopwatch.stop();
							ReportUtil.addKeyword(stepDescription, keyword, "Pass", null,stopwatch);
							testStatus.add("Pass");
						}

						// Take screenshot - only on
						// error
						if (result.startsWith("Fail")) {

							testStatus.add("Fail");

							// Give a fileName for
							// the screenshot and
							// store
							String fileName = "Suite1_TS" + tsid + "_TC" + tcid + "_" + keyword + ".jpg";
							String path = screenshotPath + fileName;
							TestUtil.takeScreenShot(path);
							APPLICATION_LOGS.debug("SCREENSHOT taken under : " + path);

							// Write the test result
							// to HTML report
							stopwatch.stop();
							ReportUtil.addKeyword(stepDescription, keyword, "Fail", fileName,stopwatch);
						}

					}

					catch (Throwable testException) {
						APPLICATION_LOGS.debug("Error came : " + testException.getMessage());
					}

				} // keywords -inner for loop

				ReportUtil.addTestCase(currentTest, testStatus.get(testStatus.size() - 1));

			}

			else {

				APPLICATION_LOGS.debug("Skipping the test : " + currentTest);
				testStatus.add("Skip");

				// Report skipped
				ReportUtil.addTestCase(currentTest, testStatus.get(testStatus.size() - 1));

			}

			if (wbdv != null) {
				/*// Logout from web portal
				FunctionLibrary.clickLink(locatorAdminDropdown, nameAdminDropdown);

				// Click on logout button
				FunctionLibrary.clickAndWait(locatorLogoutLink, nameLogoutLink);*/
				
				FunctionLibrary.closeDriver();
			}
			

			
		}

	}

	@AfterClass
	public static void endScript() throws Exception {
		//SendMail.sendEmail();
		/* server.stop();*/
		APPLICATION_LOGS.debug("*** Test run finished ***");

	}

}